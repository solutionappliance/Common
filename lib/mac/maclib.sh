#! /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# maclib.sh
####################################################################################################################################
#
# Common functions for Mac OS X.
#
####################################################################################################################################

CP_IMPORTS+="maclib;"

function showHiddenFiles() {
	log info "defaults write com.apple.finder AppleShowAllFiles ${1:-YES}"
	defaults write com.apple.finder AppleShowAllFiles ${1:-YES}
}

# Obtain a username from keychain
function getUserFromKeychain() {
	declare kcUser=$(security find-internet-password -s "$1" | grep "\"acct\"<blob>=" | cut -d'=' -f 2 | cut -c2- | rev | cut -c2- | rev)
	echo "${kcUser}"
}

# Obtain a password from keychain
function getPasswordFromKeychain() {
	declare kcPass=$(security find-internet-password -gs "$1" 2>&1 | grep "^password:" | awk '{print $2}' | tr -d '"')
	echo "${kcPass}"
}

# Obtain github.com username from keychain
function getGitHubUserFromKeychain() {
  declare ghUser=$(getUserFromKeychain "github.com")
  [[ -z ${ghUser} ]] && return 1 || echo ${ghUser}; return 0
}

# Obtain github.com password from keychain
function getGitHubPasswordFromKeychain() {
  declare ghPass=$(getPasswordFromKeychain "github.com")
  [[ -z ${ghPass} ]] && return 1 || echo ${ghPass}; return 0
}
