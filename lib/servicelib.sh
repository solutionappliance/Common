#! /bin/bash
# vim:ts=4:sw=4:nowrap:
####################################################################################################################################
# servicelib.sh
####################################################################################################################################
#
# Shell based service support.
#
####################################################################################################################################

CP_IMPORTS+="service;"
[[ -z "${CP_SERVICE_PATH:-}" ]] && export CP_SERVICE_PATH="${CP_BASE}/Common/services"


function initCommonServices() {
	declare IFS=$'\n'
	declare svcPath
	declare pretty=false
	declare verbose=false
	declare -a services=( )
	declare -a needToSource=( )
	declare json="{"

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-f)	unset CP_SERVICE_INIT;;
		-v)	verbose=true;;
		*)	log -name=registerServices error "Unknown option ${1}"; return 9;;
		esac
		shift
	done

	case "${CP_SERVICE_INIT:-z}" in
		true*)
			if [[ "${CP_SERVICE_INIT}" = "true;${CP_SERVICE_PATH}" ]]; then
				${verbose} && log -name=initCommonServices info "CP_SERVICE PATH same, exiting";
				return;
			fi
			${verbose} && log -name=initCommonServices info "CP_SERVICE_PATH has changed, updating"
			;;
		false*)
			if [[ "${CP_SERVICE_INIT}" != "false;${CP_SERVICE_PATH}" ]]; then
				${verbose} && log -name=initCommonServices info "CP_SERVICE_PATH has changed, updating"
			else
				${verbose} && log -name=initCommonServices info "CP_SERVICE PATH same, sourcing service files"
				for srcFile in ${CP_SERVICE_TOSRC[@]}; do
					source "${srcFile}" "${srcFile}"
				done
				return
			fi
			;;
		*)
			export CP_SERVICE_INIT="true;${CP_SERVICE_PATH}"
			;;
	esac

	declare -a readyServices=()
	declare -a notReadyServices=()

	for svcPath in $(echo "${CP_SERVICE_PATH}" | tr ';' '\n' ); do
		if [[ -d "${svcPath}" ]]; then
			for file in $(ls "${svcPath}"); do
				[[ -d "${svcPath}/${file}" ]] && continue
				declare svc="${file%%.*}"
				declare dep=''

				${verbose} && log info "Found ${svcPath}/${svc} @ ${file}"

				services+=( "${svc}" )
				json=$(appendJson "${json}" "${svc}" "{")
				json=$(appendJson "${json}" "path" "${svcPath}/${file}")

				if [[ -x "${svcPath}/${file}" ]]; then
					json=$(appendJson "${json}" "svcType" "script")
					for header in $(keyValueHeader -file="${svcPath}/${file}"); do
						${verbose} && log info "... ${header}"
						declare key="${header%%:*}"
						declare value="$(trim "${header#*:}")"
						json=$(appendJson "${json}" "${key}" "${value}")
						[[ "${key}" = 'dependencies' ]] && dep="${value}"
					done
				elif [[ "${file}" = *".service" ]]; then
					json=$(appendJson "${json}" "svcType" "lib")
					for header in $(${grep} '^# CommonService\.service' "${svcPath}/${file}" | sed -E -e 's/# CommonService\.service\.([^:]+):[[:space:]]*(.*)[[:space:]]*/\1:\2/'); do
						${verbose} && log info "... ${header}"
						declare key="${header%%:*}"
						declare value="$(trim "${header#*:}")"
						json=$(appendJson "${json}" "${key}" "${value}")
						[[ "${key}" = 'dependencies' ]] && dep="${value}"
					done
				else
					json=$(appendJson "${json}" "svcType" "unknown")
				fi

				if [[ -z "${dep}" ]]; then
					readyServices+=( "${svc}" )
				else
					notReadyServices+=( "${svc}" )
					dep=";${dep// /;};"
					eval "cp_svc_dep_${svc}='${dep}'"
				fi

				json=$(appendJson "${json}" "svcActive" "_${svc}_Active")
				json+="}"
			done
		fi
	done
	json+="}"


	declare -a defServices=( "${services[@]}" )
	declare -a needToSource=()
	services=()

	while [[ ! -z "${readyServices}" ]]; do
		declare -a rSvcs=( "${readyServices[@]}" )
		readyServices=()

		for svc in "${rSvcs[@]}"; do
			${verbose} && log info "Running ${svc}"
			declare active=false
			declare svcPath=$(jsonValue "${json}" ".${svc}.path")

			if [[ -x "${svcPath}" ]]; then
				declare needsInit=$(jsonValue "${json}" ".${svc}.init")
				if ${needsInit:-true}; then
					if "${svcPath}" init; then
						${verbose} && log info "${svc}: Init returned true"
						active=true

						declare bn="${svcPath%/*}"
						if [[ ":${PATH}:" != *":${bn}:" ]]; then
							export PATH="${PATH}:${bn}"
							${verbose} && log info "${svc}: Added ${bn} to path"
						fi
					else
						${verbose} && log info "${svc}: Init returned false"
					fi
				else
					active=true
				fi
			else
				if source "${svcPath}" "${svcPath}"; then
					${verbose} && log info "${svc}: Sourced script"
					needToSource+=( "${svcPath}" )
					active=true
				else
					${verbose} && log info "${svc}: Init returned false"
				fi
			fi

			if ${active}; then
				services+=( "${svc}" )
				json=${json//\"_${svc}_Active\"/true}

				declare -a nrSvcs=( "${notReadyServices[@]}"  )
				notReadyServices=()
				for nrSvc in "${nrSvcs[@]}"; do
					unset dep
					eval "dep=\${cp_svc_dep_${nrSvc}}"
					declare oldDep="${dep}"

					if [[ "${dep}" = *";${svc};"* ]]; then
						dep=";${dep//;${svc};/};"
						dep="${dep//;;/;}"

						if [[ "${dep}" = ";" ]]; then
							eval "unset cp_svc_dep_${nrSvc}"
							readyServices+=( "${nrSvc}" )
						else
							eval "cp_svc_dep_${nrSvc}='${dep}'"
							notReadyServices+=( "${nrSvc}" )
						fi
					else
						notReadyServices+=( "${nrSvc}" )
					fi
				done
			else
				json=${json//\"_${svc}_Active\"/false}
			fi
		done
	done

	for svc in "${notReadyServices[@]}"; do
		json=${json//\"_${svc}_Active\"/false}
		if ${verbose}; then
			unset dep
			eval "dep=\${cp_svc_dep_${svc}//;/ }"
			log warning "Cannot start ${svc} due to missing dependences: ${dep}"
		fi
	done

	export CP_SERVICES="${services[@]}"
	export CP_SERVICE_INFO="${json//\": /\":}"
	export CP_SERVICE_TOSRC="${needToSource[@]}"
	export CP_SERVICE_INIT="false;${CP_SERVICE_PATH}"
}

function getServiceList() {
	declare IFS=';'
	declare svcPath
	declare pretty=false

	echo "${CP_SERVICES[@]}"
}

function listServices() {
	declare IFS=$'\n'
	declare entry

	printf "${TC_FG_YELLOW}%s${TC_END}\n" "${CP_PROJECT_ACTIVE_ID_PATH:-/}"

	for entry in ${CP_SERVICES[@])}; do
        printf "${TC_FG_BLUE}%-20s${TC_END}: ${TC_FG_WHITE}%s${TC_END}\n" "${entry}" "$(jsonValue "${CP_SERVICE_INFO}" ".${entry}.name")"
    done;
	[[ -z "${entry}" ]] && return 1 || return 0
}

function _completeServiceList() {
	declare IFS=$'\n'
    COMPREPLY=( $(compgen -W '${CP_SERVICES[@]}' -- ${COMP_WORDS[COMP_CWORD]}) )
}

function upperFirstChar() {
    echo "$(echo "${1:0:1}" | tr 'a-z' 'A-Z')${1:1}"
}

function csFunctionName() {
	declare serviceName="${1}"
	declare serviceFunc="${2}"

	declare function="_commonService$(upperFirstChar "${serviceName}")$(upperFirstChar "${serviceFunc}")"
	if ! type -p "${function}"; then
		return 1
	fi

	echo "${function}"
	return 0
}

function service() {
	declare IFS=$'\n'
	declare verbose=false
	declare serviceName=''
	declare cmd="call"

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-v)	verbose=true;;
		-l)	cmd="list";;
		-s)	cmd="status";;
		*)	log error "Unknown option ${1}"; return 9;;
		esac
		shift
	done

	# Service list
	if [[ "${cmd}" == "list" ]]; then
		startSection "Service list"
		for serviceName in $(echo "${CP_SERVICE_INFO}" | jq -c 'keys'  | tr '[]' ' ' | tr ',' '\n' | cut -f2 -d\"); do
			declare svcAct=$(jsonValue "${CP_SERVICE_INFO}" ".${serviceName}.svcActive")
			declare desc=$(jsonValue "${CP_SERVICE_INFO}" ".${serviceName}.description")

			if [[ "${svcAct}" != "true" ]]; then
				printf "${TC_FG_RED}%-10s${TC_END}: %s\n" "${serviceName}" "${desc}"
			else
				printf "${TC_FG_BLUE}%-10s${TC_END}: %s\n" "${serviceName}" "${desc}"
			fi
		done
		endSection
		return 0	
	fi

	# Service status
	if [[ "${cmd}" == "status" ]]; then
		declare -i rc=0

		startSection "Service status"
		for serviceName in ${CP_SERVICES[@]}; do
			if [[ "${serviceName}" != "services" ]]; then
				declare status
				if ! status=$(SA_COLORLESS=force commonService ${serviceName} status); then
					rc=1
				fi
				printKeyValueLine "${serviceName}" "${status}"
			else
				declare message="ready"
				declare rc=0

				printKeyValueLine "${serviceName}" "${TC_FG_BLUE}${message}${TC_END}"
			fi
		done
		endSection
	
		return ${rc}
	fi

	if [[ -z "${serviceName}" ]]; then
		serviceName="${1}"; shift
	fi

	if [[ -z "${serviceName}" ]]; then
		log error "Usage service serviceName command"
		return 1
	fi

	declare function
	declare svcActive=$(jsonValue "${CP_SERVICE_INFO}" ".${serviceName}.svcActive")

	if [[ -z "${svcActive}" ]]; then
		log error "Service ${serviceName} does not exist"
		return 8
	elif ! ${svcActive}; then
		log error "Service ${serviceName} is not active"
		return 9
	fi

	case $(jsonValue "${CP_SERVICE_INFO}" ".${serviceName}.svcType") in
		"lib")
			declare serviceFunc="${1:-help}"; shift
			if ! function=$(csFunctionName "${serviceName}" "${serviceFunc}"); then
				if [[ " ${CommonService_activeServices} " != *" ${serviceName} "* ]]; then
					log error "Cannot find the ${serviceName} service"
					return 8
				fi
				log error "Cannot find the ${serviceFunc} function in the ${serviceName} service"
				return 9
			fi

			${verbose} && log info "Running ${function} ${*}"
			eval "${function}" "${@}"
			;;
		"script")
			if ! function=$(jsonValue "${CP_SERVICE_INFO}" ".${serviceName}.path"); then
				log warning "Cannot find service ${serviceName}"
				return 1
			fi
			${verbose} && log info "Running ${function} ${*}"
			"${function}" ${@}
			;;
		*)
			log error "Do not know how to handle ${serviceName}"
			;;
	esac
}

complete -F _completeServiceList service

function commonService() {
	declare serviceOrOption="${1}"
	if [[ -z "${serviceOrOption}" || "${serviceOrOption}" == "-h" ]]; then
		cat << HELP_SECTION
Description:
	Tool for running cross-platform services.

Usage:
	commonService serviceName command
	commonService option

Options:
	-h        Display this help
	-l        List services
HELP_SECTION
		return 0
	elif [[ "${serviceOrOption}" == "-l" ]]; then
		commonService services list
		return 0
	fi
	service "${@}"
}

complete -F _completeServiceList commonService
