#! /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# profilelib.sh
####################################################################################################################################
#
# Support for profiles
#
####################################################################################################################################

CP_IMPORTS+="profile;"

function initCommonProfile() {
	declare verbose=false
	declare dryRun=false
	declare defaultProfile
	declare srcProfiles=false

	while [[ "${1:-}" = -* ]]; do
		case "${1}" in
		-i|-f)		unset CP_PROFILE; export CP_PROFILE;;
		-n)			dryRun=true;;
		-v)			verbose=true;;
		-src)		srcProfiles=true;;
		-default=*)	defaultProfile="${1#*=}";;
		*)			log error "Unknown option ${1}"; return 9;;
		esac
		shift
	done

	if [[ -z "${CP_PROFILE}" ]]; then
		if [[ ! -r "${HOME}/.cpprofile" ]]; then
			if [[ ! -z "${defaultProfile}" ]]; then
				export CP_PROFILE="${defaultProfile}"
				${verbose} && log info "Could not locate ${HOME}/.cpprofile; using default ${defaultProfile}"
			fi
			${verbose} && log info "Could not locate ${HOME}/.cpprofile"
		else
			if ${dryRun}; then
				${verbose} && log info "Found profile: $(grep "^;" "${HOME}/.cpprofile" | head -1)"
			else
				export CP_PROFILE=$(grep "^;" "${HOME}/.cpprofile" | head -1)
				${verbose} && log info "Loaded ${HOME}/.cpprofile: ${CP_PROFILE}"
			fi
		fi
	else
		${verbose} && log info "Profile already initialized: ${CP_PROFILE}"
	fi

	if ${srcProfiles} && getCommonProfileFlag cp; then
		source "${CP_BASE}/Common/lib/commonlib.sh"

		${verbose} && log info "Sourcing profiles"
		! ${dryRun} && sourceProfiles

		# Additional libs?
		declare libs
		if libs=$(getCommonProfileSetting libs); then
			for srcLib in $(cpLibsToSource ${libs//,/ }); do
				source "${srcLib}" || log -name=profile error "Cannot find ${srcLib}"
			done
		fi
		
		
		# Get base project
		declare baseProject=$(getCommonProfileSetting project)
		if [[ ! -z "${CP_SETTINGS}" ]]; then
			case "$(getJsonValue "${CP_SETTINGS}" '.active.type')" in
			top)
				baseProject="$(getJsonValue -n "${CP_SETTINGS}" .active.id)"
				;;
			*)
			  ;;
			esac
		fi


		# Source the project path
		if [[ ! -z "${baseProject}" ]]; then
			if [[ "${SOURCE_BASH_PROFILE:-0}" -eq 1 ]]; then
				! ${dryRun} && sourceProjectPath -m ${baseProject}
			else
				! ${dryRun} && sourceProjectPath ${baseProject}
			fi
		fi

		# This is kinda slow
		! ${dryRun} && command -v initCommonServices &> /dev/null && initCommonServices
	fi

	return 0
}

function getCommonProfileFlag() {
	declare -a opts=()

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-i) unset CP_PROFILE; export CP_PROFILE;;
		-v)	opts+=( -v );;
		*)	log error "Unknown option ${1}"
			return 9;;
		esac
		shift
	done

	if [[ "$(getCommonProfileSetting ${opts[@]} "${1}")" = "true" ]]; then
		return 0
	else
		return 1
	fi
}

function getCommonProfileSetting() {
	declare verbose=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-i) unset CP_PROFILE; export CP_PROFILE;;
		-v)	verbose=true;;
		*)	log error "Unknown option ${1}"
			return 9;;
		esac
		shift
	done

	declare varName="${1}"
	declare -a opts=()
	${verbose} && opts+=( "-v ")

	if [[ "${CP_PROFILE}" =~ ";${1}="([^;]*)";" ]]; then
		# Primary value
		echo "${BASH_REMATCH[1]}"
		return 0
	elif [[ ! -z "${2}" ]]; then
		# Default value
		${verbose} && log info "Could not locate ${varName}, using default value \"${2}\""
		echo "${2}"
		return 0
	fi

	${verbose} && log info "Could not locate ${varName}"
	return 1
}

function getCommonProfile() {
	declare verbose=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-i) unset CP_PROFILE; export CP_PROFILE;;
		-v)	verbose=true ;;
		*)	log error "Unknown option ${1}"
			return 9;;
		esac
		shift
	done


	if ! initCommonProfile ${opts[@]}; then
		${verbose} && log info "Could not initialize profile"
		return 2
	fi

	${verbose} && log info "Profile: ${CP_PROFILE}"
	getCommonProfileSetting ${opts[@]} 'profile'
	return 0
}

function setCommonProfile() {
	declare verbose=false
	declare dryRun=false
	declare line=false
	declare IFS=$'\n'

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		# Force re-initialization
		-i) unset CP_PROFILE; export CP_PROFILE;;		
		-n)	dryRun=true;;
		-v)	verbose=true;;
		-l)	list=true;;
		*)	log error "Unknown option ${1}"
			return 9;;
		esac
		shift
	done


	if ${list}; then
		declare profiles=":"
		for file in $(resolvePath -m "${CP_ETC_PATH}" "profiles.txt"); do
			for line in $(cat "${file}" | grep -E -e "^[a-zA-Z]+" ); do
				profileName="${line%:*}"
				line="$(trim "${line#*:}")"
				if [[ "${profiles}" != *":${profileName}:"* ]]; then
					if [[ "${line}" = "${CP_PROFILE}" ]]; then
						printKeyValueLine "* ${profileName}" "${line}"
					else
						printKeyValueLine "${profileName}" "${line}"
					fi
					profiles+="${profileName}:"
				fi
			done
		done
		if [[ -z "${1}" ]]; then
			return  0
		fi
	fi

	declare profileName="${1}"
	declare profileLine

	function findProfileLine() {
		declare file
		for file in $(resolvePath -m "${CP_ETC_PATH}" "profiles.txt"); do
			${verbose} && log info "Checking ${file}"
			if profileLine=$(${grep} -i -E "^${profileName}:" "${file}" | head -1); then
				profileLine="$(trim "${profileLine#*:}")"
				[[ ! -z "${profileLine}" ]] && return 0
			fi 
		done
		return 1
	}

	if ! findProfileLine; then
		log error "Could not find profile with name ${profileName}"
		return 1
	fi

	${verbose} && log info "Using ${profileLine}"
	if ! ${dryRun}; then
		echo "${profileLine}" > "${HOME}/.cpprofile"
	fi

	return 0
}

function initCommonPaths() {
	declare cfgDir="${1}"
	declare env=${CP_ENVS:-$(getCommonProfileSetting env)}
	declare -a envs=( "." ${env//[,;]/$'\n'} )
	declare etcDir libDir tDir binDir svcDir

	for id in ${envs[@]}; do
		if [[ -z "${id}" ]] || [[ "${id}" = '.' ]]; then
			etcDir="${cfgDir}/etc"
			libDir="${cfgDir}/lib"
			tDir="${cfgDir}/template"
			binDir="${cfgDir}/bin"
			svcDir="${cfgDir}/services"
		else
			etcDir="${cfgDir}/etc/${id}"
			libDir="${cfgDir}/lib/${id}"
			tDir="${cfgDir}/template/${id}"
			binDir="${cfgDir}/bin/${id}"
			svcDir="${cfgDir}/services/${id}"
		fi

		[[ -d "${binDir}" ]] && [[ ":${PATH}:" != *":${binDir}:"* ]] && export PATH="${binDir}:${PATH}"
		[[ -d "${etcDir}" ]] && [[ ";${CP_ETC_PATH};" != *";${etcDir};"* ]] && export CP_ETC_PATH="${etcDir};${CP_ETC_PATH}"
		[[ -d "${libDir}" ]] && [[ ";${CP_LIB_PATH};" != *";${libDir};"* ]] && export CP_LIB_PATH="${libDir};${CP_LIB_PATH}"
		[[ -d "${tDir}" ]] && [[ ";${CP_TEMPLATE_PATH};" != *";${tDir};"* ]] && export CP_TEMPLATE_PATH="${tDir};${CP_TEMPLATE_PATH}"
		[[ -d "${svcDir}" ]] && [[ ";${CP_SERVICE_PATH};" != *";${svcDir};"* ]] && export CP_SERVICE_PATH="${svcDir};${CP_SERVICE_PATH}"
	done
}

function sourceProfiles() {
	declare IFS=$'\n'

	# Other configurations
	declare profiles="$(getCommonProfileSetting profiles)"
	[[ ",${profiles}," != *",Common,"* ]] && profiles="Common,${profiles}"
	for cfg in ${profiles//,/$'\n'}; do
		declare cfgDir="${CP_BASE}/${cfg}"

		if [[ -d "${cfgDir}" ]]; then
			initCommonPaths "${cfgDir}" "${id}"
		fi

		if [[ -e "${cfgDir}/lib/setupenv.sh" ]]; then
			source "${cfgDir}/lib/setupenv.sh"
		fi
	done

	# Other libraries
	for srcLib in $(cpLibsToSource $(getCommonProfileSetting lib | tr ',' '\n') ); do
		if ! source "${srcLib}"; then
			log error "Failed to source ${srcLib}"
			exit 1
		fi
	done
}
