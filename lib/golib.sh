# /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# golib.sh
####################################################################################################################################
#
# Provides a "go" command
#
####################################################################################################################################

CP_IMPORTS+="go;"

function getGoEntry() {
	declare searchKey="${1}"

	declare cpVarKey="CP_GO_${searchKey}"
	declare cpVarValue="${!cpVarKey}"

	if [[ ! -z "${cpVarValue}" ]]; then
		echo "${cpVarValue}"
		return 0
	fi

	return 1
}

function getGoKeys() {
	env | grep -e '^CP_GO_' | cut -f1 -d= | cut -f3- -d_ | sort
}

function getGoList() {
	declare IFS=$'\n'
	declare goEntry
	declare goKey

	for goKey in $(getGoKeys); do
		declare cpVarKey="CP_GO_${goKey}"
		declare cpVarValue="${!cpVarKey}"
		echo "${goKey}=${cpVarValue}"
	done
}

function _completeGoList() {
	declare IFS=$'\n'
	declare -a list=()
	declare goValue
	if ! [[ "${COMP_WORDS[COMP_CWORD]}" = */* ]]; then
		list=( $(getGoKeys) )
	else
		dir="${COMP_WORDS[COMP_CWORD]}"
		goKey="${dir%%/*}"
		if ! goValue="$(getGoEntry "${goKey}")"; then 
			COMPREPLY=()
			return 1
		fi

		dir="${dir/${goKey}/${goValue}}"
		dir="${dir%/*}"

		for entry in $(find "${dir}" -type d -maxdepth 1);  do
			value="${entry/${goValue}/${goKey}}"
			list+=( "${value}" )
		done
	fi

	COMPREPLY=( $(compgen -W '${list[@]}' -- ${COMP_WORDS[COMP_CWORD]}) )
}

function dir() {
	declare goKey
	declare goValue
	declare path
	declare silent=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-q|-s)	silent=true;;
		*)
			log error "Unknown option ${1}"
			return 9
			;;
		esac
		shift
	done

	goKey="${1%%/*}"
	path="${1/${goKey}/}"

	if ! goValue="$(getGoEntry "${goKey}")"; then 
		! ${silent} && log error "dir: No entry for ${goKey}"
		return 1
	fi

	echo "${goValue}${path}"
}


function go() {
	declare IFS=$'\n'
	declare silent=false
	declare goMode='cd'
	declare goEntry
	declare goValue
	declare goKey
	declare goClear=false
	declare verbose=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-v)			verbose=true;;
		-mode=*)	goMode="${1#*=}";;
		-q)			silent="true";;
		-check)		goMode="check";;
		-c)			goClear=true;;
		-o|-cd)		goMode="cd";;
		-p)			goMode="print";;
		-h|-help)	goMode="help";;
		-l|-list)	goMode="list";;
		-i|-int) 	goMode="interactive";;
		-)			goMode="last";;
		-push)		goMode="push";;
		-pop)		goMode="pop";;
		-v) 		verbose=true;;
		-stack)		goMode="stack";;
		*)		log error "go: Unknown option ${1}";
					return 9;;
		esac
		shift
	done

	goKey="${1:-root}"
	${goClear} && export CP_GO_POP="" && export CP_GO_LAST=""

	case "${goMode}" in
	help)
			printKeyValueLine "-help" "Display help"
			printKeyValueLine "-" "Pop directory from stack"
			printKeyValueLine "-l" "List go short cuts"
			printKeyValueLine "-i" "Interactively list go short cuts"
			printKeyValueLine "-stack" "Display stack"
			printKeyValueLine "-p" "Print directory"
			return 0
			;;
	list)
			for goEntry in $(getGoList | sort); do
				declare goKey="${goEntry%=*}"
				declare goValue="${goEntry#*=}"
				if [[ "${goValue}" = *'${'* ]]; then
					goValue=$(eval "echo \"${goValue}\"")
				fi

				printKeyValueLine "${goKey}" "${goValue}"
			done
			return 0
			;;
	last)
			goValue="${CP_GO_LAST}"
			export CP_GO_LAST="${PWD}"

			${silent} || log -name=go info "Go ${goValue}"
			cd "${goValue}"

			return 0
			;;

	push)
			if [[ ";${CP_GO_POP}" != *";${PWD}" ]]; then
				export CP_GO_POP="${CP_GO_POP:+${CP_GO_POP};}${PWD}"
			fi

			return 0
			;;
	pop)
			if [[ "${CP_GO_POP}" = *";"* ]]; then
				goValue="${CP_GO_POP##*;}"
				export CP_GO_POP="${CP_GO_POP%;*}"
			else
				goValue="${CP_GO_POP}"
				export CP_GO_POP=""
			fi

			if [[ ! -z "${goValue}" ]]; then
				${silent} || log -name=go info "Go ${goValue}"
				cd "${goValue}"
			fi

			return 0
			;;

	stack)
			declare IFS=";"
			for goValue in ${CP_GO_POP}; do
				echo "${goValue}"
			done
			echo "${PWD}"
			return 0
			;;
	esac

	if ! goValue="$(dir -q "${goKey}")"; then
		log error "go: No entry for ${goKey}"
		return 2
	fi

	${verbose} && log -name=go info "Found ${goValue} for ${goKey}"
	declare fqp
	if ! fqp=$(fullyQualifiedPath "${goValue}"); then 
		log error "go: No path for ${goValue}"
		return 2
	fi
	${verbose} && log -name=go info "Qualified ${fqp} for ${goValue}"

	case "${goMode}" in
	print)
		echo "${fqp}"
		return 0
		;;
	check)
		;;
	cd)
		if [[ ";${CP_GO_LAST}" != *";${PWD}" ]]; then
			export CP_GO_LAST="${PWD}"
		fi
		${silent} || log info "Go ${fqp}"
		cd "${fqp}";;
	*)
		log error "go: Unknown mode ${goMode}"; return 2;;
	esac

	return 0
}

complete -F _completeGoList go
complete -F _completeGoList dir
