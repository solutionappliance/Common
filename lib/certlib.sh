#! /bin/bash
################################################################################
# certlib.sh
################################################################################
#
# Common functions for certificate handling
#
################################################################################

CP_IMPORTS+="cert;"



################################################################################
# certKeystoreCheck
#-------------------------------------------------------------------------------
#
# Is cert alias in keystore? Return 0 if true. 3 arguments required:
# - 1 keystore path
# - 2 keystore password
# - 3 cert alias
#
################################################################################
function certKeystoreCheck() {
  if [[ $# -ne 3 ]]; then
    echo "Is a cert in a keystore? Return 0 if true."
    echo "3 arguments required: keystore path, keystore password, cert alias."
    echo "Example: certKeystoreCheck ${JAVA_HOME}/jre/lib/security/cacerts changeit \"verisignclass2g2ca [jdk]\""
    return 1;
  fi

  declare kPath="$1"
  declare kPass="$2"
  declare cAlias="$3"

  if [[ ! -f "${kPath}" ]]; then
    log error "keystore file not found: '${kPath}'"
    return 1
  fi

  log info "Checking if '${cAlias}' is in '${kPath}'"
  declare findAlias=$(sudo keytool -list -keystore ${kPath} -storepass ${kPass} -v -alias "${cAlias}")
  if [[ ${findAlias} == "Alias name: ${cAlias}"* ]]; then
    log info "Alias '${cAlias}' found in '${kPath}'"
    return 0
  else
    log info "Alias '${cAlias}' not found in '${kPath}'"
    return 1
  fi
}

################################################################################
# certKeystoreAdd
#-------------------------------------------------------------------------------
#
# Add cert to keystore. 4 arguments required:
# - 1 keystore path
# - 2 keystore password
# - 3 cert alias
# - 4 cert path
#
################################################################################
function certKeystoreAdd() {
  if [[ $# -ne 4 ]]; then
    echo "Add a cert to a keystore."
    echo "4 arguments required: keystore path, keystore password, cert alias, cert path."
    echo "Example: certKeystoreAdd ${JAVA_HOME}/jre/lib/security/cacerts changeit \"my-cert\" ~/my-cert.crt"
    return 1;
  fi

  declare kPath="$1"
  declare kPass="$2"
  declare cAlias="$3"
  declare cPath="$4"

  if [[ ! -f "${kPath}" ]]; then
    log error "keystore file not found: '${kPath}'"
    return 1
  fi
  if [[ ! -f "${cPath}" ]]; then
    log error "cert file not found: '${cPath}'"
    return 1
  fi

  if certKeystoreCheck "${kPath}" "${kPass}" "${cAlias}"; then
    log info "Alias '${cAlias}' found, so will not import into '${kPath}'"
  else
    log info "Alias '${cAlias}' not found, so importing '${cAlias}' into '${kPath}'"
    yes | sudo keytool -importcert -file ${cPath} -alias "${cAlias}" -keystore ${kPath} -storepass ${kPass} &>/dev/null;
  fi

}

################################################################################
# certKeystoreDelete
#-------------------------------------------------------------------------------
#
# Delete cert in keystore. 3 arguments required:
# - 1 keystore path
# - 2 keystore password
# - 3 cert alias
#
################################################################################
function certKeystoreDelete() {
  if [[ $# -ne 3 ]]; then
    echo "Delete a cert from a keystore."
    echo "3 arguments required: keystore path, keystore password, cert alias."
    echo "Example: certKeystoreDelete ${JAVA_HOME}/jre/lib/security/cacerts changeit \"my-cert\""
    return 1;
  fi

  declare kPath="$1"
  declare kPass="$2"
  declare cAlias="$3"

  if [[ ! -f "${kPath}" ]]; then
    log error "keystore file not found: '${kPath}'"
    return 1
  fi

  if certKeystoreCheck "${kPath}" "${kPass}" "${cAlias}"; then
    log info "Alias '${cAlias}' found, so deleting '${cAlias}' from '${kPath}'"
    sudo keytool -delete -alias "${cAlias}" -keystore ${kPath} -storepass ${kPass}
  else
    log info "Alias '${cAlias}' not found, so will not delete from '${kPath}'"
  fi
}

################################################################################
# certKeystoreList
#-------------------------------------------------------------------------------
#
# List certs in keystore. 2 arguments required:
# - 1 keystore path
# - 2 keystore password
#
################################################################################
function certKeystoreList() {
  if [[ $# -ne 2 ]]; then
    echo "List certs in keystore."
    echo "2 arguments required: keystore path, keystore password."
    echo "Example: certKeystoreList ${JAVA_HOME}/jre/lib/security/cacerts changeit"
    return 1;
  fi

  declare kPath="$1"
  declare kPass="$2"

  if [[ ! -f "${kPath}" ]]; then
    log error "keystore file not found: '${kPath}'"
    return 1
  fi

  sudo keytool -list -keystore ${kPath} -storepass ${kPass}
}
