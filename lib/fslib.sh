#! /bin/bash
# vim:sw=4:ts=4:softtabstop=4:noexpandtab:
####################################################################################################################################
# File System helper utilities
####################################################################################################################################
#
#
####################################################################################################################################

CP_IMPORTS+="fs;"

lsMounts() {
	case "${CP_ENV}" in
	linux)
		mount -l | sed -E -e 's/([^ ]*) on ([^ ]*) type ([^ ]*).*\((.*)\).*/;mp=\2;dev=\1;type=\3;opt=\4;/'
		;;
	mac)
		mount | sed -E -e 's/(.*) on ([^(]*) \(([^,]*), (.*)\).*/;mp=\2;dev=\1;type=\3;opt=\4;/ ; s/, /,/g'
		;;
	*)
		log error "do not know how to get mounts for ${CP_ENV}"
		return 1
		;;
	esac
}
