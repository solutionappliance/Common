#! /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# commonlib.sh
####################################################################################################################################
#
# Common functions.
#
####################################################################################################################################

CP_IMPORTS+="common;"

case "${SHELL}" in
	*/bash)	CP_SHELL='bash';;
	*/zsh)	CP_SHELL='zsh';;
	*)		CP_SHELL='sh';;
esac

[[ -z "${CP_BASE}" ]] && export CP_BASE="${HOME}/Profile"
[[ -z "${CP_COMMON}" ]] && export CP_COMMON="${CP_BASE}/Common"

export CP_GO_cp="${CP_COMMON}"
export CP_ETC_PATH="${CP_COMMON}/etc"
export CP_LIB_PATH="${CP_COMMON}/lib"
export CP_TEMPLATE_PATH="${CP_COMMON}/template"
export CP_BIN_MAC_PATH="${CP_COMMON}/bin/mac"
export CP_ITERM_PATH="${HOME}/.iterm2"

[[ -d "${HOME}/etc/Install" ]] && export CP_INSTALL_PATH="${HOME}/etc/Install;${CP_INSTALL_PATH}"

if [[ -z "${CP_ENV:-}" ]]; then
	export CP_ENV="$(getCommonProfileSetting env 2> /dev/null)"
	if [[ -z "${CP_ENV}" ]]; then
		case $(uname -s) in
		'Darwin') export CP_ENV="mac" ;;
		'Linux') export CP_ENV='linux' ;;
		esac
	fi
fi

if [[ -z "${CP_ENVS:-}" ]]; then
	export CP_ENVS="${CP_ENV};${CP_ENV}/$(uname -m)"
fi

if [[ -d "${CP_COMMON}/template/${CP_ENV}" ]] && [[ ";${CP_TEMPLATE_PATH};" != *";${CP_COMMON}/template/${CP_ENV};"* ]]; then
	export CP_TEMPLATE_PATH="${CP_COMMON}/template/${CP_ENV};${CP_TEMPLATE_PATH}"
fi

case "${CP_ENV:-}" in
	'mac')
		[[ -z "${CP_CREDENTIALS_DEFAULTS_FILE}" ]] && export CP_CREDENTIALS_DEFAULTS_FILE="${HOME}/Library/Preferences/CommonProfile.credentials"
		[[ -z "${CP_SYSTEM_ID:-}" ]] && export CP_SYSTEM_ID="$(ioreg -c "IOPlatformExpertDevice" | awk -F '"' '/IOPlatformSerialNumber/ {print $4}')"
		;;
esac


function initTerminalCodes() {
	if [ -t 1 ] && [[ "${SA_COLORLESS:-false}" != "true" ]] || [[ "${SA_COLORLESS}" = "force" ]]; then
		if  [[ ${TC_INIT} -ne 1 ]]; then
			export TC_INIT=1

			export TC_B=$(tput bold)
			export TC_BLINK=$(tput blink)
			export TC_UL=$(tput smul)
			export TC_END_UL=$(tput rmul)
			export TC_END=$(tput sgr0)

			export TC_CLR_EOL=$(tput el)
			export TC_SC=$(tput sc)
			export TC_RC=$(tput rc)

			export TC_BG_BLACK=$(tput setab 0)
			export TC_BG_RED=$(tput setab 1)
			export TC_BG_GREEN=$(tput setab 2)
			export TC_BG_YELLOW=$(tput setab 3)
			export TC_BG_BLUE=$(tput setab 4)
			export TC_BG_MAGENTA=$(tput setab 5)
			export TC_BG_CYAN=$(tput setab 6)
			export TC_BG_WHITE=$(tput setab 7)

			export TC_FG_BLACK=$(tput setaf 0)
			export TC_FG_RED=$(tput setaf 1)
			export TC_FG_GREEN=$(tput setaf 2)
			export TC_FG_YELLOW=$(tput setaf 3)
			export TC_FG_BLUE=$(tput setaf 4)
			export TC_FG_MAGENTA=$(tput setaf 5)
			export TC_FG_CYAN=$(tput setaf 6)
			export TC_FG_WHITE=$(tput setaf 7)
		fi
	else
		if  [[ ${TC_INIT} -ne 2 ]]; then
			export TC_INIT=2

			export TC_B=''
			export TC_BLINK=''
			export TC_UL=''
			export TC_END_UL=''
			export TC_END=''

			export TC_CLR_EOL=''
			export TC_SC=''
			export TC_RC=''

			export TC_BG_BLACK=''
			export TC_BG_RED=''
			export TC_BG_GREEN=''
			export TC_BG_YELLOW=''
			export TC_BG_BLUE=''
			export TC_BG_MAGENTA=''
			export TC_BG_CYAN=''
			export TC_BG_WHITE=''

			export TC_FG_BLACK=''
			export TC_FG_RED=''
			export TC_FG_GREEN=''
			export TC_FG_YELLOW=''
			export TC_FG_BLUE=''
			export TC_FG_MAGENTA=''
			export TC_FG_CYAN=''
			export TC_FG_WHITE=''
		fi
	fi
}

initTerminalCodes

export TC_ALERT="${TC_FG_RED}"
export TC_NORMAL="${TC_FG_BLACK}"
export TC_BOLD="${TC_FG_GREEN}"
export TC_KEY="${TC_FG_BLUE}"

LL_FATAL=(	10 	"${TC_FG_RED}${TC_BLINK}fatal   ${TC_END}")
LL_ERROR=(	20 	"${TC_FG_RED}error   ${TC_END}")
LL_WARNING=(	30	"${TC_FG_RED}warn    ${TC_END}")
LL_INFO=(	40	"${TC_FG_GREEN}info    ${TC_END}")
LL_DEBUG=(	50	"${TC_FG_BLUE}debug   ${TC_END}")

export LL_FATAL LL_ERROR LL_WARNING LL_INFO LL_DEBUG


#--------------------------------------------------------------------------------
#  Trims a string of leading and trailing whitespace
#--------------------------------------------------------------------------------
# Example:
#    var=$(trim ${var})
#
function trim() {
	declare var=$@
	var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
	var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
	echo -n "$var"
}

#--------------------------------------------------------------------------------
#  getPid <searchString>
#--------------------------------------------------------------------------------
# Return the first PID of any matching processes
#
# Example:
#	pid=$(getPid java Application)
#
function getPid() {
	declare pid=$(psaux "$@" | awk '{print $2}' | head -1)
	if [[ ! -z "${pid}" ]]; then
		echo "${pid}"
		return 0
	fi
	return 1
}

#--------------------------------------------------------------------------------
#  getPids <searchString>
#--------------------------------------------------------------------------------
# Return the PID(s) of any/all matching processes
#
# Example:
#	pids=( $(getPid java Application) )
function getPids() {
	declare pids=$(psaux "$@" | awk '{print $2}')
	if [[ ! -z "${pids}" ]]; then
		echo "${pids}"
		return 0
	fi
	return 1
}

#--------------------------------------------------------------------------------
#  waitForProcess maxSeconds <searchString>
#--------------------------------------------------------------------------------
# Return true if process is running, false otherwise
#
function waitForProcess() {
	declare -i tries="${1}"; shift
	while [[ ${tries} -ge 0 ]]; do
		let tries--
		declare pid=$(psaux "$@" | awk '{print $2}' | head -1)
		if [[ ! -z "${pid}" ]]; then
			return 0
		fi
		sleep 1
	done

	return 1
}

#--------------------------------------------------------------------------------
#  waitForProcessToEnd maxSeconds <searchString>
#--------------------------------------------------------------------------------
# Return true if process is no longer running, false otherwise
#
function waitForProcessToEnd() {
	declare -i tries="${1}"; shift
	while [[ ${tries} -ge 0 ]]; do
		let tries--
		declare pid=$(psaux "$@" | awk '{print $2}' | head -1)
		if [[ -z "${pid}" ]]; then
			return 0
		fi
		sleep 1
	done

	return 1
}



#--------------------------------------------------------------------------------
#  psaux [regex]*
#--------------------------------------------------------------------------------
# Performs a ps aux and supports zero, one, or more argments to grep for.
#
function psaux() {
	declare verbose=false
	declare include="!/$$/"

	if [[ "${1}" = '-v' ]]; then
		verbose=true
		shift
	fi

	while [[ ! -z "${1}" ]]; do
		prefix=''

		case "${1}" in
		-e|--include)	
			shift;;
		--exclude) 
			prefix='!'
			shift
			;;
		*)	
			;;
		esac

		include+="${include:+ && }${prefix}/${1//\//\/}/"
		shift
	done

	if [[ -z "${include}" ]]; then
		${verbose} && log info "ps aux"
		ps aux
	else
		${verbose} && log info "ps aux | awk ${include}"
		ps aux | awk "${include}" 
	fi
}


#--------------------------------------------------------------------------------
#  startSection <title>
#--------------------------------------------------------------------------------
# Prints a section header across stdout
#
function startSection() {
	[[ -z "$COLUMNS" ]] && COLUMNS=$(tput cols)
	declare -i cols=${COLUMNS}
	[[ ${cols} -gt 132 ]] && cols=132
	echo ""

	echo $(seq -s= ${COLUMNS} | tr -d '[:digit:]')
	echo "${*}"
	echo $(seq -s- ${COLUMNS} | tr -d '[:digit:]')
}

#--------------------------------------------------------------------------------
#  endSection
#--------------------------------------------------------------------------------
# Prints a section footer across stdout
#
function endSection() {
	echo ""
}


#--------------------------------------------------------------------------------
#  printKeyValueLine
#--------------------------------------------------------------------------------
# Prints a single line of help or command line usage
#
function printKeyValueLine() {
	printfln "${TC_KEY}%-20s${TC_END} %s" "${1}" "${2}"
}

#--------------------------------------------------------------------------------
#  printlnError <text>
#--------------------------------------------------------------------------------
# Prints a line of text to stderr
#
function printlnError() {
	echo "$@" 1>&2
}

#--------------------------------------------------------------------------------
#  println <text>
#--------------------------------------------------------------------------------
# Prints a line of text to stdout
#
function println() {
	echo "$@"
}

function printfln() {
	printf "$@"
	echo ""
}

function printflnError() {
	printf "$@" 1>&2
	echo "" 1>&2
}

#--------------------------------------------------------------------------------
#  setTerminalTabName <title>
#--------------------------------------------------------------------------------
# Sets the title of the terminal tab if running iTerm or iTerm2.
#
function setTerminalTabName {
	if [[ ${TERM_PROGRAM} == "iTerm.app" ]]; then
		echo -ne "\033]0;$*\007"
	elif [[ "${TERM}" == "xterm"* ]]; then
		echo -ne "\033]0;$*\007"
	fi
}

#--------------------------------------------------------------------------------
#  requestTerminalAttention
#--------------------------------------------------------------------------------
# Causes the iTerm/iTerm2 application to bounce if the current window is not
# the iTerm/iTerm2 window.
#
function requestTerminalAttention {
	if [[ ${TERM_PROGRAM} == "iTerm.app" ]]; then
		echo -ne "\033]50;RequestAttention=true\007"
	fi
}

#--------------------------------------------------------------------------------
#  clearScrollBack
#--------------------------------------------------------------------------------
# Causes the iTerm/iTerm2 application to clear the screen and scrollback buffer
#
function clearScrollBack {
	if [[ ${TERM_PROGRAM} == "iTerm.app" ]]; then
		echo -ne "\033]1337;ClearScrollback\007"
	fi
}

#--------------------------------------------------------------------------------
#  requestTerminalFocus
#--------------------------------------------------------------------------------
# Gives the iTerm/iTerm2 application window the keyboard focus
#
function requestTerminalFocus {
	if [[ ${TERM_PROGRAM} == "iTerm.app" ]]; then
		echo -ne "\033]50;StealFocus\007"
	fi
}

#--------------------------------------------------------------------------------
#  copyToClipboard <text>
#--------------------------------------------------------------------------------
# Copies the specified text to the general clipboard (iTerm/iTerm2 only)
#
function copyToClipboard() {
	if [ -t 1 ]; then
		if [[ ${TERM_PROGRAM} == "iTerm.app" ]]; then
			echo -ne "\033]50;CopyToClipboard\007"
			echo -n "$*"
			echo -ne "\033]50;EndCopy\007"
			echo ""
			return 0
		fi
	fi
	echo "$*"
	return 0
}

#--------------------------------------------------------------------------------
#  Get the fully-qualified path name of the specified directory or file.
#--------------------------------------------------------------------------------
#
function fullyQualifiedPath() {
	declare file=''
	declare verbose=false
	declare resolveLinks=false

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-v) verbose=true;;
		-l)	resolveLinks=true;;
		*)
			if [[ -z "${file}" ]] && [[ "${file}" != -* ]]; then 
				file="${1}"
			else
				log error "Unsupported option ${1}"
				return 9
			fi
			;;
		esac
		shift 
	done

	if [[ "${file}" = *'${'* ]]; then
		file=$(eval "echo \"${file}\"")
	fi

	${verbose} && log -name=fullyQualifiedPath info "Looking for ${file}"

	if [[ "${file}" = "smb://"* ]] || [[ "${file}" = "afp://"* ]]; then
		remoteMount "${file}"
		return $?
	fi

	if [[ -d "${file}" ]]; then
		if ${resolveLinks}; then
			while [[ ! -z "${file}" ]] && [[ -L "${file}" ]]; do
				file="$(readlink "${file}")"
				${verbose} && log info "Followed link to ${file}"
			done
		fi

		pushd "${file}" > /dev/null
		file=$(pwd -P)
		popd > /dev/null
	
		echo "${file}"
		return 0

	elif [[ -e "${file}" ]]; then
		if ${resolveLinks}; then
			while [[ ! -z "${file}" ]] && [[ -L "${file}" ]]; do
				declare newFile="$(readlink "${file}")"
				[[ "${newFile}" != /* ]] && file="$(dirname "${file}")/${newFile}" || file="${newFile}"
				${verbose} && log info "Followed file link to ${file}"
			done
		fi

		fn=$(basename "${file}")
		dir=$(dirname "${file}")

		if ${resolveLinks}; then
			while [[ ! -z "${dir}" ]] && [[ -L "${dir}" ]]; do
				declare newDir="$(readlink "${dir}")"
				[[ "${newDir}" != /* ]] && dir="${dir}/${newDir}" || dir="${newDir}"
				${verbose} && log info "v2 ... Followed ${newDir} link to ${dir}"
			done
		fi

		pushd "$dir" > /dev/null
		file="$(pwd -P)/${fn}"
		popd > /dev/null

		echo "${file}"
		return 0
	fi

	return 1
}


#--------------------------------------------------------------------------------
# Read all file header key/value pairs
#--------------------------------------------------------------------------------
# Returns all entries from the start of a file inbetween the first two
# sets of ###'s
#
# Example:
# #! /bin/file
# ########################
# # myHeader
# # Test
# ########################
# File
#
# This function will return ( "myHeader" "Test" )
#
function keyValueHeader() {
	declare -a opts=()
	declare keyValuesOnly=true

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-all)
			keyValuesOnly=false
			;;
		-)
			opts=( cat )
			;;

		-file=*)
			declare file="${1#*=}"
			if [[ ! -e "${file}" ]]; then
				log warning "Cannot find file ${file}"
				return 1
			else
				opts=( cat "${file}" )
			fi
			;;

		*)
			opts=( echo "${1}" )
			;;
		esac
		shift
	done

	if ${keyValuesOnly}; then
		${opts[@]} | sed -E -e '/^[[:space:]]*$/,$d' -e '/^[^#]/,$d' -e '1,/^###*/d' -e '/^###/,$d' -e 's/^#[[:space:]]*//' -e '/^$/d' -e 's/[[:space:]]*:[[:space:]]*/:/' | ${grep} --color=never -E '[a-zA-Z0-9._]*:.*' | grep --color=never -v '^#'
	else
		${opts[@]} | sed -E -e '/^[[:space:]]*$/,$d' -e '/^[^#]/,$d' -e '1,/^###*/d' -e '/^###/,$d' -e 's/^#[[:space:]]*//' -e '/^$/d' -e 's/[[:space:]]*:[[:space:]]*/:/' | grep --color=never -v '^#'
	fi
}

#--------------------------------------------------------------------------------
# sectionHeader
#--------------------------------------------------------------------------------
# Isolates the text a [section] inside of a file header.  The file header
# is all of the text denoted between ####...'s at the start of a file.
#
function sectionHeader() {
	declare -a opts=()
	declare section="${1}"; shift

	case "${1}" in
	-)
		opts=( cat )
		;;

	-file=*)
		declare file="${1#*=}"
		if [[ ! -e "${file}" ]]; then
			log warning "Cannot find file ${file}"
			return 1
		else
			opts=( cat "${file}" )
		fi
		;;

	*)
		opts=( echo "${1}" )
		;;
	esac

#	${opts[@]} | sed -E -e '/^[^#]/,$d' -e "1,/^#[[:space:]]*\\[${section}]/d" -e '/^#[[:space:]]*\[/d' -e '/^###/,$d' -e 's/^#[[:space:]]*//'
	${opts[@]} | ${sed:-sed} -E -e "1,/^#[[:space:]]*\\[${section}]/d" -e  '/^#[[:space:]]*\[/d; /^###/,$d; s/^#[[:space:]]*//; s/^\.([[:space:]])/ \1/'
}

#--------------------------------------------------------------------------------
# Get a value from set of "key=value" pairs stored in an array
#--------------------------------------------------------------------------------
# Usage:
#   person=("firstName=Bob" lastName="Smith" fullName="Bob Smith")
#   if personName=$(getMapValue firstName "${person[@]}"); then
#     echo "Person's first name is $personName"
#   fi
#
function getMapValue() {
	shopt -s nocasematch
	declare var="${1}"; shift 1
	declare -a array=("${@}")
	declare entry

	for entry in "${array[@]}"; do
		if [[ "${entry}" =~ ^"${var}"[[:space:]]*:[[:space:]]*(.*) ]]; then
			echo "${BASH_REMATCH[1]}"
			shopt -u nocasematch
			return 0
		elif [[ "${entry}" =~ ^"${var}"$ ]]; then
			echo "true"
			shopt -u nocasematch
			return 0
		fi
	done

	shopt -u nocasematch
	return 1
}

#--------------------------------------------------------------------------------
# log level message
#--------------------------------------------------------------------------------
# Simple logging, taking a level and data to print
#
function log() {
	declare logLevelParm
	declare logName=${cpLogId:-}
	declare -i minLogLevel=${cpLogLevel:-40}
	declare verbose=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-v)
			verbose=true;;
		-name=*)
			logName="${1#*=}";;
		-level-*)
			logLevelParm="${1#*=}";;
		*)
			log -name=log error "Unknown option ${1}"
			return 1
			;;
		esac
		shift 1
	done

	[[ -z "${logLevelParm}" ]] && logLevelParm="${1}"; shift 1
	declare logLevelKey=LL_$(echo "${logLevelParm}" | tr '[:lower:]' '[:upper:]')[@]
	declare logLevel=("${!logLevelKey}")

	if [[ ${minLogLevel} -ge ${logLevel[0]:-3} ]]; then
		if [[ -z "${logName}" ]]; then
			printfln "%-8s: %s" "${logLevel[1]:-${logLevelParm}}" "$*" 1>&2
		else
			printfln "%-8s: %s %s" "${logLevel[1]:-${logLevelParm}}" "${TC_FG_BLUE}${logName}${TC_END}" "$*" 1>&2
		fi
	fi
}

#--------------------------------------------------------------------------------
# varSub [-f] "templateText" ["name: value"]*
#--------------------------------------------------------------------------------
# Replace all variables of the form $[name:opt] with their associated values
# from the name:value map.
#
# Adding the -f flag will remove all $[name:opt] variables from the text,
# replacing them with their opt "debug=xxx" value or "".
#
function varSub() {
	declare -i finalize=0
	if [[ "${1}" == '-f' ]]; then finalize=1; shift 1; fi
	declare result="${1}"; shift 1
	declare -a vars=("${@}")
	declare -i rc=0
	declare IFS=$'\n'

	for var in $(echo "${result}" | grep -hoe '$\[[^]]*\]' | cut -f2 -d'[' | cut -f1 -d']'); do
		declare varName varType value
		if [[ "${var}" =~ ([^:\]*):(.*) ]]; then

			varName="${BASH_REMATCH[1]}"
			varType="${BASH_REMATCH[2]}"
		else
			varName="${var}"
			varType='single'
		fi

		# Not final
		if [[ ${finalize} -eq 0 ]]; then
			# Has value
			if value=$(getMapValue "${varName}" "${vars[@]}"); then
				case "${varType}" in
					single | single-required | required | optionalLine | default=*)
						result="${result//\$\[${var}\]/${value}}"
						;;
					multi | multi-required)
						result="${result//\$\[${var}\]/${value}\$[${varName}:multi]}"
						;;
				esac
			fi

		# Finalize
		else
			# Has value
			if value=$(getMapValue "${varName}" "${vars[@]}"); then
				result="${result//\$\[${var}\]/${value}}"

			# No value
			else
				case "${varType}" in
					default=*)
						result="${result//\$\[${var}\]/${varType#default=*}}"
						;;
					optionalLine)
						result=$(echo "${result}" | sed "/\$\[${var}\]/d");;
					single | multi)
						result="${result//\$\[${var}\]/}"
						;;
					single-required | required | multi-required)
						rc=1
						;;
				esac
			fi
		fi
	done

	echo "$result"
	return ${rc}
}



#--------------------------------------------------------------------------------
#  copy_function <from> <to>
#--------------------------------------------------------------------------------
# Copies the named function to a new name
#
function copy_function() {
	test -n "$(declare -f $1)" || return
	eval "${_/$1/$2}"
}

#--------------------------------------------------------------------------------
#  rename_function <from> <to>
#--------------------------------------------------------------------------------
# Copies the named function to a new name and removes the old name.
#
function rename_function() {
	copy_function $@ || return
	unset -f $1
}

function jsonValue() {
	declare value=$(echo "${1}" | jq "${2}")
	if [[ "${value}" = 'null' ]]; then
		return 1
	fi

	[[ "${value}" =~ \"(.*)\" ]] && value="${BASH_REMATCH[1]}"
	value="${value//\\r/}"
	value="${value//\\n/$'\n'}"
	echo -n "${value}"
}

function ensureSession() {
	if [[ -z "${COMMONPROFILE_SESSION_KEY}" || -z "${COMMONPROFILE_SESSION_DATA}" ]]; then
		declare password=$(security find-generic-password -ga $(whoami) -s commonprofile.session 2>&1 | grep "password:" | cut -f2 -d\")
		if [[ -z "${password}" ]]; then
			password=$(dd if=/dev/random bs=1 count=256 2> /dev/null | base64)
			security add-generic-password -a $(whoami) -s commonprofile.session -w "${password}"
		fi

		export COMMONPROFILE_SESSION_KEY="$(dd if=/dev/urandom bs=1 count=128 2> /dev/null | base64)"
		export COMMONPROFILE_SESSION_DATA=$(echo "${password}" | openssl aes-256-cbc -a -salt -k "${COMMONPROFILE_SESSION_KEY}")
	fi

	return 0
}

#--------------------------------------------------------------------------------
# endSession
#--------------------------------------------------------------------------------
#  End a user session
#
function endSession() {
	unset COMMONPROFILE_SESSION_KEY
	unset COMMONPROFILE_SESSION_DATA
	export COMMONPROFILE_SESSION_KEY
	export COMMONPROFILE_SESSION_DATA
}


#--------------------------------------------------------------------------------
# enc=$(echo ... | sessionEncrypt)
#--------------------------------------------------------------------------------
# Encrypt data using the password provided when sessionStart was called
#
function sessionEncrypt() {
	ensureSession
	declare password=$(echo "${COMMONPROFILE_SESSION_DATA}" | openssl aes-256-cbc -d -a -k "${COMMONPROFILE_SESSION_KEY}" 2> /dev/null)
	openssl aes-256-cbc -a -salt -k "${password}" 2> /dev/null
	return $?
}

#--------------------------------------------------------------------------------
# dec=$(echo ... | sessionDecrypt)
#--------------------------------------------------------------------------------
# Decrypt data using the password provided when sessionStart was called
#
function sessionDecrypt() {
	ensureSession
	declare password=$(echo "${COMMONPROFILE_SESSION_DATA}" | openssl aes-256-cbc -d -a -k "${COMMONPROFILE_SESSION_KEY}" 2> /dev/null)
	openssl aes-256-cbc -d -a -k "${password}" 2> /dev/null
	return $?
}

function openTerminal() {
	declare title
	declare -i bg=0

	while [[ "${1}" == -* ]]; do
		case "${1}" in
		-b)		bg=1;;
		esac
		shift 1
	done

	title="${1}"; shift 1
	if [[ -z "${1}" ]]; then
		printlnError "Usage openTerminal title command"
	fi

	if [[ "${TERM_PROGRAM}" == "iTerm.app" ]]; then
		if [[ $bg -eq 1 ]]; then
		osascript &>/dev/null << END_OPEN_BG_TERMINAL
			tell application "iTerm"
				tell current window
					set thisTab to (current tab)
					set newTab to (create tab with default profile)
					tell thisTab
						select
					end tell
					tell current session of newTab
						set name to "${title}"
						write text "${*}"
					end tell
				end tell
			end tell
END_OPEN_BG_TERMINAL
		else
		osascript &>/dev/null << END_OPEN_TERMINAL
			tell application "iTerm"
				tell current window
					set newTab to (create tab with default profile)
					tell current session of newTab
						set name to "${title}"
						write text "${*}"
					end tell
				end tell
			end tell
END_OPEN_TERMINAL
		fi
	elif [[ -z "${1}" ]]; then
		terminal --title "${title}"
	else
		terminal --title "${title}" -e "$@"
	fi
}

function afpMount() {
	declare endPoint="${1}"
	declare serverMount serverMountSearch restOfPath

	if [[ "${endPoint}" =~ ^"afp://"([^/]+)/([^/]+)(.*)$ ]]; then
		serverMount="${BASH_REMATCH[1]}/${BASH_REMATCH[2]}"
		endPoint="afp://${serverMount}"
		serverMountSearch="${BASH_REMATCH[1]}\/${BASH_REMATCH[2]} on"
		restOfPath="${BASH_REMATCH[3]}"
	else
		log -name="afpMount" error "afpMount: Unsupport end point: ${endPoint}"
		return 5
	fi

	declare mountPoint=$(mount | grep "${serverMountSearch}" | head -1 | awk '{print $3}')
	if [[ ! -z "${mountPoint}" ]]; then
		echo -n "${mountPoint}${restOfPath}"
		return 0
	fi

	declare -i tryNo=0
	declare -i maxTry=100
	log -name="afpMount" warning "afpMount: Please open ${endPoint}"
	open -g "${endPoint}"
	while [[ -z "${mountPoint}" && ${tryNo} -lt ${maxTry} ]]; do
		sleep 1
		mountPoint=$(mount | grep "${serverMountSearch}" | head -1 | awk '{print $3}')
		let tryNo++
	done

	if [[ ! -z "${mountPoint}" ]]; then
		echo -n "${mountPoint}${restOfPath}"
		return 0
	fi
	return 1
}

function remoteMount() {
	declare endPoint="${1}"
	declare serverMount serverMountSearch restOfPath

	if [[ "${endPoint}" =~ ^(smb|afp)"://"([^/]+)/([^/]+)(.*)$ ]]; then
		serverMount="${BASH_REMATCH[2]}/${BASH_REMATCH[3]}"
		endPoint="${BASH_REMATCH[1]}://${serverMount}"
		serverMountSearch="${BASH_REMATCH[2]}\/${BASH_REMATCH[3]} on"
		restOfPath="${BASH_REMATCH[4]}"
	else
		log -name="remoteMount" error "Unsupport end point: ${endPoint}"
		return 5
	fi

	declare mountPoint=$(mount | grep "${serverMountSearch}" | head -1 | awk '{print $3}')
	if [[ ! -z "${mountPoint}" ]]; then
		echo -n "${mountPoint}${restOfPath}"
		return 0
	fi

	declare -i tryNo=0
	declare -i maxTry=100
	open -g "${endPoint}"
	while [[ -z "${mountPoint}" && ${tryNo} -lt ${maxTry} ]]; do
		[[ ${tryNo} -eq 5 ]] && log -name="remoutMount" info "Please open ${endPoint}"
		sleep 1
		mountPoint=$(mount | grep "${serverMountSearch}" | head -1 | awk '{print $3}')
		let tryNo++
	done

	if [[ ! -z "${mountPoint}" ]]; then
		echo -n "${mountPoint}${restOfPath}"
		return 0
	fi
	return 1
}

function csvSort() {
	declare v
	declare result
	for v in $(echo "${1}" | tr ',' $'\n' | sort); do
		result="${result:+${result},}$v"
	done
	echo "${result}"
}

# Tries several times to unmount a drive on the mac
function tryUnmount() {
	declare mp="${1}"

	declare -i tryNo=0
	while [[ ${tryNo} -lt 5 ]] && ! hdiutil detach "${mp}"; do
		let tryNo++
		sleep 5
	done

	if [[ ${tryNo} -ge 5 ]]; then
		log error "detach of ${mp} failed"
		if ! hdiutil detach -force "${mp}"; then
			return 1
		fi
	fi

	return 0
}

function setBadge() {
	printf "\e]1337;SetBadgeFormat=%s\a"   $(echo -n "${1}" | base64)
}

function getProcessReadOffset() {
	declare fileName="${1}"
	declare line

	declare output=""
	for line in $(lsof -Fpsof -oo32 "${fileName}"); do
		case "${line}" in
		p*)	! [[ -z "${output}" ]] && echo "${output}"
			output="${line#p*}:$(stat -f '%z' "${fileName}")"
			;;

		o0t*) output="${output}:${line#o0t*}" ;;

		f*);;
		s*);;

		*)	log -name="getProcessReadOffset" error "Unexpected line ${line}";;
		esac
	done

	! [[ -z "${output}" ]] && echo "${output}"
}

function show() {
	declare showBinary=false
	declare showText=true
	declare showLocation=false
	declare clipLocation=false
	declare openAtom=false
	declare showPretty=true
	declare showImages=true
	declare pager

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-h|--help)
			printfln "show - Display a script, function, or other resource,"
			printfln "       or information such as location."
			printfln "       By default, showText=true, showPretty=true, showImages=true."
			printKeyValueLine "-h, --help" "Display this help"
			printKeyValueLine "-r, --raw" "Display raw (showPretty=false, showImages=false)"
			printKeyValueLine "-f, --format" "Format (showPretty=true)"
			printKeyValueLine "-i, --images" "Images (showImages=true)"
			printKeyValueLine "-ni, --noimages" "No images (showImages=false)"
			printKeyValueLine "-T, --only-text" "Only text (showText=true, showPretty=true, showLocation=false, showBinary=false, showImages=false)"
			printKeyValueLine "-B, --only-binary" "Only binary (showLocation=false, showBinary=true, showText=false, showIages=false, showPretty=false)"
			printKeyValueLine "-t, --text" "Text (showText=true)"
			printKeyValueLine "-nt, --no-text" "No text (showText=false)"
			printKeyValueLine "-b, --binary" "Binary (showBinary=true)"
			printKeyValueLine "-nb, --no-binary" "No binary (showBinary=false)"
			printKeyValueLine "-l, --location" "Location (showLocation=true)"
			printKeyValueLine "-nl, --no-location" "No location (showLocation=false)"
			printKeyValueLine "-L, --only-location" "Only location (showLocation=true,showBinary=false,showText=false,showPretty=false,showImages=false)"
			printKeyValueLine "-cl, --clip-location" "Show location and copy location to clipboard"
			printKeyValueLine "-a, --atom" "Open location in atom editor"
			return 0
		;;
		-r|--raw)
			showPretty=false
			showImages=false
			;;
		-f|--format)
			showPretty=true
			;;
		-i|--images)
			showImages=true
			;;
		-ni|--noimages)
			showImages=false
			;;
		-T|--only-text)
			showText=true
			showPretty=true
			showLocation=false
			showBinary=false
			showImages=false
			;;
		-B|--only-binary)
			showLocation=false
			showBinary=true
			showText=false
			showImages=false
			showPretty=false
			;;
		-t|--text)
			showText=true
			;;
		-nt|--no-text)
			showText=false
			;;
		-b|--binary)
			showBinary=true
			;;
		-nb|--no-binary)
			showBinary=false
			;;
		-l|--location)
			showLocation=true
			;;
		-nl|--no-location)
			showLocation=false
			;;
		-L|--only-location)
			showLocation=true
			showBinary=false
			showText=false
			showPretty=false
			showImages=false
			;;
		-cl|--clip-location)
			clipLocation=true
			showLocation=true
			showBinary=false
			showText=false
			showPretty=false
			showImages=false
			;;
		-a|--atom)
			openAtom=true
			showLocation=false
			showBinary=false
			showText=false
			showPretty=false
			showImages=false
			;;
		*) echo "Option ${1} is not supported"  1>&2
			return 1;;
		esac
		shift 1
	done

	declare source="${1}"
	declare sourceType

	if [[ "${source}" = 'file://'* ]]; then
		source="${source#file://*}"
	elif [[ "${source}" = 'file:'* ]]; then
		source="${source#file:*}"
	fi

	if [[ "${source}" = 'http:'* ]]; then
		sourceType='www'
	elif [[ -f "${source}" ]]; then
		sourceType='file'
	else
		sourceType=$(type -t "${source}")
		case "${sourceType}" in
		file)
			source=$(type -p "${source}" | head -1)
		esac
	fi


	case "${sourceType}" in
	www)
		declare IFS=$'\n'
		declare url="${source}"
		declare -a options=()
		declare payload
		declare httpResponse
		declare httpHeader
		declare httpBody

		options+=( "-s" "-D-" )

		if [[ ! -z "${payload}" ]]; then
			echo "${payload}" | jq .
			httpResponse="$(echo "${payload}" | curl "${options[@]}" "${url}" -d@- )"
		else
			httpResponse="$(curl "${options[@]}" "${url}")"
		fi

		if [[ "$(echo "${httpResponse}" | head -1)" =~ HTTP/([0-9.]*)[[:space:]]([0-9]*)[[:space:]](.*) ]]; then
			httpHeader+=( "Http-Version:${BASH_REMATCH[1]}" "Http-Code:${BASH_REMATCH[2]}" "Http-Message:${BASH_REMATCH[3]}" )
		fi
		httpHeader+=( $(echo "${httpResponse}" | sed -E -n -e '2,/^[[:space:]]*$/p' -e '/^[[:space:]]*$/d') )
		httpBody=$(echo "${httpResponse}" | sed '1,/^[[:space:]]*$/d')

		declare -i httpReturnCode="$(getMapValue "Http-Code" "${httpHeader[@]}")"
		declare httpMessage="$(getMapValue "Http-Message" "${httpHeader[@]}")"

		if [[ ${httpReturnCode} -ge 200 && ${httpReturnCode} -le 299 ]]; then
			log -name="show" info "${httpReturnCode} Success: ${httpMessage}"
		else
			log -name="show" error "${httpReturnCode} Failed: ${httpMessage}"
		fi

		echo "${httpHeader[*]}"
		echo ""


		if [[ "$(getMapValue "Content-Type" "${httpHeader[@]}")" = "application/json"* ]]; then
			echo "${httpBody}" | jq .
		else
			echo "${httpBody}"
		fi
		;;

	file)
		if ${showLocation}; then
			echo "${source}"
			${clipLocation} && echo -n "${source}" | pbcopy;
		fi
		if ${openAtom}; then
			if command -v atom &> /dev/null; then
				open -a atom "${source}"
			else
				echo "Atom not installed. It can be installed with: brew install --cask atom"
			fi
		fi

		declare fileType=$(file -b "${source}")
		declare mimeEncoding=$(file -b --mime-encoding "${source}")

		if [[ "${mimeEncoding}" = "binary" ]]; then
			if ${showPretty}; then
				case "${fileType}" in
					"OpenPGP Secret Key"*)
						cat "${source}" | gpg --list-packets
						return 0
						;;
				esac
			fi

			if ${showImages}; then
				if [[ "${source}" = *.jpg ]]; then cat "${source}" | ${CP_ITERM_PATH}/imgcat
				elif [[ "${source}" = *.png ]]; then cat "${source}" | ${CP_ITERM_PATH}/imgcat
				elif ${showBinary}; then hexdump -C "${source}"
				elif "${showText}"; then echo "${source} is binary"  1>&2
				fi
			elif ${showBinary}; then
				hexdump -C "${source}"
			elif ${showText}; then
				echo "${source} is binary"  1>&2
			fi
		else
			if ${showPretty}; then
				case "${fileType}" in
				"OpenSSH RSA public key")
					ssh-keygen -f ${source} -e -m pkcs8  | openssl rsa -noout -text -inform pkcs8 -pubin
					ssh-keygen -lf "${source}"  -E md5
					;;
				"OpenSSH private key")
					ssh-keygen -f ${source} -e -m pkcs8  | openssl rsa -noout -text -inform pkcs8
					ssh-keygen -lf "${source}"  -E md5
					;;


				"PEM RSA private key")
					openssl rsa -in "${source}"  -noout -text
					ssh-keygen -lf "${source}"  -E md5
					;;

				*)
					case "${source}" in
					*.json)
						cat "${source}" | jq . | highlight --out-format=ansi --syntax=json
						;;
					*.xml)
						cat "${source}" | xmllint --format - | highlight --out-format=ansi --syntax=xml
						;;
					*.md)
						pandoc "${source}" | lynx -stdin -dump
						;;
					*.html)
						cat "${source}" | lynx -stdin -dump
						;;
					*.pem|*.key)
						openssl rsa -in "${source}"  -noout -text
						;;
					*.pub)
						openssl rsa -noout -text -inform PEM -in "${source}" -pubin
						;;
					*.cer | *.crt | *.cert)
						openssl x509 -in "${source}"  -noout -text -fingerprint
						;;
					*.csr)
						openssl req -in "${source}" -noout -text
						;;
					*.dot)
						${showImages} && dot "${source}" -Tpng|  ${CP_ITERM_PATH}/imgcat || highlight --out-format=ansi --force "${source}" 2> /dev/null
						;;
					*)
						highlight --out-format=ansi --force "${source}" 2> /dev/null
						;;
					esac
				;;
			esac
			elif "${showText}"; then
				cat "${source}"
			fi
		fi
		;;
	function)
		if ${showLocation}; then
			declare funcSource
			funcSource=$(shopt -s extdebug; declare -F "${source}"; shopt -u extdebug)
			declare funcSourceFile=${funcSource#* * }
			echo "${funcSourceFile}"
			${clipLocation} && echo -n "${funcSourceFile}" | pbcopy;
		fi
		if ${openAtom}; then
			if command -v atom &> /dev/null; then
				declare funcSource
				funcSource=$(shopt -s extdebug; declare -F "${source}"; shopt -u extdebug)
				declare funcSourceFile=${funcSource#* * }
				open -a atom "${funcSourceFile}"
			else
				echo "Atom not installed. It can be installed with: brew install --cask atom"
			fi
		fi
		if "${showText}"; then
			if ${showPretty}; then
				type "${source}" | grep -v  "^${source} is a function$" | highlight --out-format=ansi --syntax=bash
			else
				type "${source}" | grep -v  "^${source} is a function$"
			fi
		fi
		;;

	*)
		if [[ -z "${sourceType}" ]]; then
			echo "${source} is not supported (-h for help)"  1>&2
		else
			echo "${sourceType} ${source} is not supported (-h for help)"  1>&2
		fi
		return 2;;
	esac
}

complete -Afunction -Afile -Abuiltin -Acommand show

#--------------------------------------------------------------------------------
#  setCredentials <key> <value>
#--------------------------------------------------------------------------------
# Returns the credentials for the specified key.  If the key is specific to a user
# ("prod" for instance), then the encrypted url will be pulled from the
# CP_CREDENTIALS_DEFAULTS_FILE and decrypted using the session key.
#
function setCredentials() {
	declare key="${1}"
	declare url="${2}"

	if [[ -z "${key}" || -z "${url}" ]]; then
		printlnError  "Please specify key and a credentials string"
		return 1
	fi

	url=$(echo "${url}" | sessionEncrypt)
	defaults write "${CP_CREDENTIALS_DEFAULTS_FILE}" "${key}" "${url}"
	return $?
}

#--------------------------------------------------------------------------------
#  getCredentials <key>
#--------------------------------------------------------------------------------
# Returns the JDBC URL for the specified key.  If the key is specific to a user
# ("prod" for instance), then the encrypted url will be pulled from the
# CP_CREDENTIALS_DEFAULTS_FILE and decrypted using the session key.
#
function getCredentials() {
	declare mode='default'
	declare clip=false
	declare print=false
	declare silent=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-h|help)
			printfln "getCredentials - obtain stored credentials based on key"
			printKeyValueLine "help, -h" "Display this help"
			printKeyValueLine "-l" "List credential keys"
			printKeyValueLine "-s KEY" "Print value associated with key"
			printKeyValueLine "-c KEY" "Copy value associated with key to clipboard"
			printKeyValueLine "-cs KEY" "Print value associated with key and copy value to clipboard"
			printKeyValueLine "-q KEY" "Silent mode"
			printfln "See commonlib.sh for other options"
			return 0
		;;
		-l)
			defaults read "${CP_CREDENTIALS_DEFAULTS_FILE}" | grep " = "  | cut -f1 -d'='  | sed '/ */s///g' | cut -f2 -d\"
			return 0;;
		-q) silent=true;;
		-c) clip=true;;
		-s) print=true;;
		-cs) clip=true; print=true;;
		-e)	mode='edit';;
		-f) mode='full';;
		-u) mode='user';;
		-j) mode='json';;
		-p) mode='pass';;
		-s)	mode='server';;
		esac
		shift 1
	done

	declare key="${1}"; shift
	declare jsonKey
	declare credentials

	if [[ ! -z "${1}" ]] && [[ "${mode}" = 'default' ]]; then
		mode="json"
	fi


	if [[ -z "${key}" ]]; then
		printlnError  "Please specify a URL key (-h for help)"
		return 1
	fi

	credentials=$(defaults read "${CP_CREDENTIALS_DEFAULTS_FILE}" "${key}" 2> /dev/null | sessionDecrypt)
	if [[ -z "${credentials}" ]] && [[ "${mode}" != 'edit' ]]; then
		! ${silent} && log -name="getCredentials" error "Unknown key ${key}"
		return 1
	fi

	if [[ "${mode}" = 'default' ]]; then
		if [[ "${credentials}" = '{'*'}' ]]; then
			mode='json'
		fi
	fi

	case "${mode}" in
	user)
		credentials="${credentials%%@*}"
		${clip} && echo -n "${credentials%%/*}" | pbcopy
		${print} && echo "${credentials%%/*}"
		;;
	pass)
		credentials="${credentials%%@*}"
		${clip} && echo -n "${credentials#*/}" | pbcopy
		${print} && echo "${credentials#*/}"
		;;
	server)
		${clip} &&
		echo -n "${credentials#*@}" | pbcopy
		${print} && echo "${credentials#*@}"
		;;
	json)
		declare jsonKey="${1}"
		if [[ ! -z "${jsonKey}" ]]; then
			declare value="$(echo "${credentials}" | jq ".${jsonKey}")"
			if [[ "${value}" = 'null' ]]; then
				! ${silent} && log -name="getCredentials" error "Unknown key ${key}/${jsonKey}"
				return 1;
			fi;
			# Escaped text
			if [[ "${value}" =~ \"(.*)\" ]]; then
				value="${BASH_REMATCH[1]}";
				value="${value//\\\\/\\}"
			fi
			${clip} && echo -n "${value}" | pbcopy
			${print} && echo "${value}"
		else
			${clip} && echo -n "${credentials}" | jq . | pbcopy
			${print} && echo "${credentials}" | jq .
		fi
		;;
	edit)
		echo "${credentials}" > ~/tmp/credentials.json
		"${EDITOR:-vi}" ~/tmp/credentials.json
		declare newCredentials="$(cat ~/tmp/credentials.json)"
		rm -f ~/tmp/credentials.json
		if [[ ! -z "$(trim "${newCredentials}")" ]] && [[ "${newCredentials}" != "${credentials}" ]]; then
			log info "Updating"
			defaults write "${CP_CREDENTIALS_DEFAULTS_FILE}" "${key}" "$(echo "${newCredentials}" | sessionEncrypt)"
			return 0
		else
			log info "Not updating"
			return 1
		fi
		;;
	full|*)
		${clip} && echo -n "${credentials}" | pbcopy
		${print} && echo "${credentials}"
		;;
	esac

	return 0
}

function rmCredentials() {
	while [[ ! -z "${1}" ]]; do
		declare key="${1}"
		shift 1


		defaults delete "${CP_CREDENTIALS_DEFAULTS_FILE}" "${key}"
	done
}

function resolvePath() {
	declare IFS=';';
	declare CHECKPATH
	declare multiple=false
	declare reverse=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-m)	multiple=true;;
		-r)	reverse=true;;
		-s=*)
			IFS="${1#*=}"
			;;

		-h)
			log info "resolvePath [-m] [-s=.] [-h] path file"
			log info "  -m   Return multiple matches"
			log info "  -s=. Specify path separator character (default=';')"
			log info "  -h   This help"
			;;

		*)	log info "resolvePath: Unknown option ${1}"
			return 9
			;;
		esac
		shift 1
	done

	declare file="${2}"
	declare results=""

	for CHECKPATH in ${1}; do
		declare result=''

		if [[ "${CHECKPATH}" = "afp:"* ]]; then
			CHECKPATH=$(afpMount "${CHECKPATH}")
		fi

		if [[ ! -z "${file}" ]]; then
			if [[ -e "${CHECKPATH}/${file}" ]]; then
				result="${CHECKPATH}/${file}"
			fi
		elif [[ -d "${CHECKPATH}" ]]; then
			result="${CHECKPATH}"
		fi

		if [[ ! -z "${result}" ]]; then
			if ${reverse}; then
				if ${multiple}; then
					results="${result}${results:+$'\n'${results}}"
				else
					results="${result}"
				fi
			else
				if ${multiple}; then
					results="${results:+${results}$'\n'}${result}"
				else
					echo "${result}"
					return 0
				fi
			fi
		fi
	done

	[[ -z "${results}" ]] && return 1

	echo "${results}"
	return 0
}

#--------------------------------------------------------------------------------
# Returns path of all requested libraries that need to be sourced
#--------------------------------------------------------------------------------
# Example:
#	declare srcLib
#	for srcLib in $(cpLibsToSource curl markdown json); do
#		[[ -e "${srcLib}" ]] && source "${srcLib}" || exit 1
#	done
#
function cpLibsToSource() {
	while [[ ! -z "${1}" ]]; do
		declare libKey="${1}"
		declare libName="${1}lib.sh"
		declare lib

		if [[ ";${CP_IMPORTS};" != *";${libKey};"* ]]; then
			if ! lib=$(resolvePath "${CP_LIB_PATH}" "${libName}"); then
				log -name=cpLibsToSource error "Cannot resolve ${libName}"
				echo "${libKey}"
			else
				echo "${lib}"
			fi
		fi

		shift
	done
}

function ctime() {
	perl -leprint\ scalar\ localtime\ "${@}"
}

#--------------------------------------------------------------------------------
#  findDuplicateFileNames <location> [<pattern>]
#--------------------------------------------------------------------------------
# Displays duplicate file names and the number of their occurrences in a location.
# Note: this implementation is very slow for large numbers of files.
function findDuplicateFileNames() {
	declare location="${1}"
	declare pattern="${2}"

	if [[ -z "${location}" ]]; then
		printlnError  "Please specify location where to search for duplicate file names"
		printlnError  "Optionally, a file pattern (ex, \"*.java\") can also be specified"
		printlnError  "Examples:"
		printlnError  "    findDuplicateFileNames src"
		printlnError  "    findDuplicateFileNames . *.java"
		printlnError  "Note: this implementation is very slow for large numbers of files."
		return 1
	fi

	if [[ -z "${pattern}" ]]; then
		find ${location} -type f -exec sh -c 'basename ${0}' {} \; | sort | uniq -c | sort -r | awk 'int($1)>=2'
	else
		find ${location} -iname "${pattern}" -type f -exec sh -c 'basename ${0}' {} \; | sort | uniq -c | sort -r | awk 'int($1)>=2'
	fi
}

#--------------------------------------------------------------------------------
#  findLargestFiles <location> [<numberOfFiles>]
#--------------------------------------------------------------------------------
# Displays the largest files in a directory and its subdirectories.
function findLargestFiles() {
	declare location="${1}"
	declare numberOfFiles="${2}"

	if [[ -z "${location}" ]]; then
		printlnError  "Please specify location where to search for largest files"
		printlnError  "By default, the top 30 files will be displayed"
		printlnError  "Optionally, the number of files to display can also be specified"
		printlnError  "Examples:"
		printlnError  "    findLargestFiles ."
		printlnError  "    findLargestFiles . 20"
		return 1
	fi
	if [[ -z "${numberOfFiles}" ]]; then
		numberOfFiles=30
	fi

	find ${location} -type f -print0 | xargs -0 ls -lha | awk '{print $5 " " $9}' | sort -hr | head -${numberOfFiles}
}

#--------------------------------------------------------------------------------
# addToPathIfNeeded
#--------------------------------------------------------------------------------
# Adds a file/directory to a path if it isn't already in the path and if it exists.
function addToPathIfNeeded() {
	declare verbose=false
	declare front=false
	declare sep=';'
	declare cpLogId=addToPathIfNeeded

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-sep=*)		sep="${1#*=}";;
		-v)			declare cpLogLevel=${LL_DEBUG};;
		-front)		front=true;;
		esac
		shift
	done

	declare pathVar="${1}"
	declare entry="${2}"
	declare path="${!1}"
	declare newPath

	if [[ -e "${entry}" ]] && [[ "${sep}${path}${sep}" != *"${sep}${entry}${sep}"* ]]; then
		if [[ -z "${path}" ]]; then
			newPath="${entry}"
		elif ${front}; then
			newPath="${entry}${sep}${path}"
		else
			newPath="${path}${sep}${entry}"
		fi

		eval "export ${pathVar}='${newPath}'"
		log debug "${pathVar} = ${newPath}"
		return 0
	fi
	return 1
}

#--------------------------------------------------------------------------------
# remoteSystemName
#--------------------------------------------------------------------------------
# Name of this system, if remote

function remoteSystemName() {
    if [[ -z "${SSH_CLIENT}" ]]; then
		return 1
	fi

	if [[ -z "${CP_REMOTE_SYSNAME}" ]]; then
        export CP_REMOTE_SYSNAME="$(echo "${SSH_CLIENT}" | awk '{print $1}')"
	fi

	echo "${CP_REMOTE_SYSNAME}"
	return 0
}

#--------------------------------------------------------------------------------
# urlEncode
#--------------------------------------------------------------------------------
# Encodes a string for a url parameter

function urlEncode() {
	echo -n "${*}" |  jq -sRr @uri
}

[[ -z "${grep}" ]] && export grep=$(resolvePath -s=':' "${PATH}" ggrep || resolvePath -s=':' "${PATH}" grep || echo 'grep')
[[ -z "${find}" ]] && export find=$(resolvePath -s=':' "${PATH}" gfind || resolvePath -s=':' "${PATH}" find || echo 'find')
[[ -z "${sed}" ]] && export sed=$(resolvePath -s=':' "${PATH}" gsed || resolvePath -s=':' "${PATH}" sed || echo 'sed')


#--------------------------------------------------------------------------------
# base64 URL encode
#--------------------------------------------------------------------------------
# base64-url encodes data from either standard in (no arguments) or from
# the argument string.
function b64url {
	declare op="enc"
	declare src=''

	while [[ "${1}" = -* ]]; do
		case "${1}" in
			-e)	op='enc';;
			-d) op='dec';;
			*)
				log error "Unsupport option ${1}"
				;;	
		esac
		shift
	done

	if [[ ! -z "${1}" ]]; then
		src="${1}"
	fi

	if [[ "${op}" = "enc" ]]; then
		if [[ -z "${src}" ]]; then
			cat | base64 | tr '/+' '_-' | tr -d '='
		else
			echo -n "${src}" | base64 | tr '/+' '_-' | tr -d '='
		fi
	else
		declare done=false

		[[ -z "${src}" ]] && src="$(cat)"
		src="${src// /}"
		src="${src//$'\n'/}"

		while [[ $(( ${#src} % 4 )) -ne 0 ]]; do
			src+='='
		done

		echo "${src}" | tr '_-' '/+' | base64 --decode 
	fi
}

#--------------------------------------------------------------------------------
# Generates a Sha256Key compatible code
#--------------------------------------------------------------------------------
# base64-url encodes data from either standard in (no arguments) or from
# the argument string.
function sha256key {
	declare src=''
	if [[ ! -z "${1}" ]]; then
		src="${1}"
	fi

	if [[ -z "${src}" ]]; then
		echo "1f$(cat | shasum -a 256 | awk '{print $1}')" | xxd -r -p | b64url -e
	else
		echo "1f$(echo -n "${src}" | shasum -a 256 | awk '{print $1}')" | xxd -r -p | b64url -e
	fi
}

#--------------------------------------------------------------------------------
# Generates a base64 url encoded sha-256 digest of the data
#--------------------------------------------------------------------------------
function sha256 {
	declare src=''
	declare srcFile=''
	declare b64=true
	declare verbose=false

	while [[ "${1}" = -* ]]; do
		case "${1}" in
			-v)		verbose=true;;
			-in=*)	srcFile="${1#*=}";;
			-hex)	b64=false;;
			*)
				log error "Unsupport option ${1}"
				return 1
				;;	
		esac
		shift
	done

	if [[ ! -z "${1}" ]]; then
		src="${1}"
		srcFile=''
	fi

	if [[ ! -z "${srcFile}" ]]; then
		if ${b64}; then
			${verbose} && log info "shasum -a 256 ${srcFile} | awk '{print $1}'"
			shasum -a 256 "${srcFile}" | awk '{print $1}' | xxd -r -p | b64url -e
		else
			${verbose} && log info "shasum -a 256 ${srcFile} | awk '{print $1}'"
			shasum -a 256 "${srcFile}" | awk '{print $1}'
		fi
	else
		if ${b64}; then
			echo -n "${src}" | shasum -a 256 | awk '{print $1}' | xxd -r -p | b64url -e
		else
			echo -n "${src}" | shasum -a 256 | awk '{print $1}'
		fi
	fi
}

#--------------------------------------------------------------------------------
# Converts a hex string back into data
#--------------------------------------------------------------------------------
function hex {
	declare src=''
	declare decode=false
	declare -a opts=()

	while [[ "${1}" = -* ]]; do
		case "${1}" in
			-e)	decode=false;;
			-d) decode=true;;
			*)
				log error "Unsupport option ${1}"
				return 1
				;;	
		esac
		shift
	done

	if [[ ! -z "${1}" ]]; then
		src="${1}"
	fi

	${decode} && opts+=( -r )


	if [[ -z "${src}" ]]; then
		cat | xxd "${opts[@]}" -p | tr -d '\n'
	else
		echo -n "${src}" | xxd "${opts[@]}" -p
	fi
}
