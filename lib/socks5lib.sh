#! /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# socks5.sh
####################################################################################################################################
#
# Socks functions.
#
####################################################################################################################################

CP_IMPORTS+="socks5;"


#--------------------------------------------------------------------------------
# openSocks5Tunnel proxyHost localPort
#--------------------------------------------------------------------------------
# Open a SOCKS5 tunnel.  The tunnel will listen on the specified localHost and
# will tunnel through the specified proxyHost.
#
function openSocks5Tunnel() {
    declare proxyHost="${1}"
    declare proxyPort=${2}
    declare pid

    if ! getPid "ssh -D ${proxyPort}" >& /dev/null; then
        ssh -D ${proxyPort} -o ExitOnForwardFailure=yes -N -f ${proxyHost}
		declare -i tryNo=0 ; while ! pid=$(getPid "ssh -D ${proxyPort}") && [[ ((++tryNo -le 20)) ]]; do sleep 1; done

		pid=$(getPid "ssh -D ${proxyPort}")
		log -name="openSocks5Tunnel" info "Started SOCKS5 tunnel via ${proxyHost}, listening on 127.0.0.1:${proxyPort} with pid ${pid}"
	else
		pid=$(getPid "ssh -D ${proxyPort}")
		log -name="openSocks5Tunnel" info "SOCKS5 tunnel already open via ${proxyHost}, listening on 127.0.0.1:${proxyPort} with pid ${pid}"
    fi

}

#--------------------------------------------------------------------------------
# stopSocksTunnel proxyHost localPort
#--------------------------------------------------------------------------------
# Close a SOCKS5 tunnel opened using openSocks5Tunnel.
#
function closeSocks5Tunnel() {
    declare proxyHost="${1}"
    declare proxyPort=${2}
    declare pid

    if pid=$(getPid "ssh -D ${proxyPort}"); then
		log -name="closeSocks5Tunnel" info "Closing SOCKS5 tunnel listening on 127.0.0.1:${proxyPort} with pid ${pid}"
        kill ${pid}
		declare -i tryNo=0 ; while getPid "ssh -D ${proxyPort}" &> /dev/null && [[ ((++tryNo -le 20)) ]]; do sleep 1; done
	else
		log -name="closeSocks5Tunnel" debug "No open SOCKS5 tunnel ${proxyHost} / ${proxyPort}"
    fi
}

