#! /bin/bash
# vim: set ts=4 : set set sw=4 :
####################################################################################################################################
# gitlib.sh
####################################################################################################################################
#
# Collection of GIT tools
#
####################################################################################################################################

CP_IMPORTS+="git;"

export GIT_VERSION=$(git --version | awk '{print $3}')

# Repo Path
function getGitRepoPath() {
	declare repo=''
	declare default=''
	declare server=''

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-github)	
			server="git@github.com";;
		*)
			log error "Unsupported option ${1}"
			return 1;;
		esac

		shift
	done

	[[ -z "${repo}" ]] && repo="${1}"
	[[ -z "${default}" ]] && default="${2:-${SA_GITREPO_TEMPLATE}}"

	if [[ ! -z "${default}" ]]; then
		varSub "${default}" "repo:${repo}"
	else
		return 1
    fi

    return 0
}

function gitGc() {
	git reflog expire --all --expire=now
	git fsck --unreachable 
	git gc --prune=now
	[[ -d .git ]] && find .git -name unhandled.\* -exec rm -f {} \;
}

function gitLsAssumeUnchanged() {
	git ls-files -v | grep ^[a-z]
}

function gitAssumeUnchanged() {
	git update-index --assume-unchanged	${@}
}

function gitNoAssumeUnchanged() {
	git update-index --no-assume-unchanged	${@}
}

function runGit() {
	if [[ "${1}" = '-dir='* ]] ; then
		declare dir="${1#*=}"
		shift 1

		if [[ ! -z "${dir}" && "${dir}" != "." ]]; then
			git -C "${dir}" ${@}
		else
			git ${@}
		fi
	else
	git ${@}
	fi
}

function getGitStatus() {
	declare IFS=$'\n'
	declare srcDir
	declare doFetch=false
	declare verbose=false
	declare mainBranch=''
	declare remote='origin'
	declare readOnly=false
	declare ignored=false
	declare clean=true
	declare allCurrent=true
	declare status
	declare message="{"

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-dir=*|-p=*)	srcDir="${1#*=}";;
		-remote=*) 		remote="${1#*=}";;
		-f|-fetch)		doFetch=true;;
		-v)				verbose=true;;
		-main=*) 		mainBranch="${1#*=}";;
		*) 				log -name=getGitStatus error "Unknown option ${1}"; 
						return 9 ;;
		esac
		shift
	done

	if [[ ! -z "${srcDir}" ]]; then
		if [[ ! -d "${srcDir}/.git" ]]; then
			log info "${srcDir} is not a git repo"
			return 1
		fi
	fi

	srcDir=$(runGit -dir="${srcDir}" rev-parse --show-toplevel)
	message=$(appendJson "${message}" "rootDir" "${srcDir}" )
	

	declare projectName="${srcDir##*/}"
	message=$(appendJson "${message}" "project" "${projectName}" )


	# Are we using main, master, or something else?
	if [[ -z "${mainBranch}" ]]; then
		if runGit -dir="${srcDir}" show-branch main &> /dev/null; then
			mainBranch=main
		elif runGit -dir="${srcDir}" show-branch master &> /dev/null; then
			mainBranch=master
		else
			log -name=getGitStatus warning "Cannot determine main branch for ${srcDir}"
			return 1
		fi
	fi

	declare mode="$(runGit -dir="${srcDir}" config --local --get user.mode)"
	if [[ ! -z "${mode}" ]]; then
		case "${mode}" in
		'readonly')		readOnly=true;;
		'ignored')		ignored=true;;
		*) 				log -name="${projectName}" warning "Unknown repo mode '${mode}'"
		esac
	fi

	message=$(appendJson "${message}" "mode" '{' )
	message=$(appendJson "${message}" "value" "${mode}" )
	message=$(appendJson "${message}" "readOnly" "${readOnly}" )
	message=$(appendJson "${message}" "ignored" "${ignored}" )
	message+="}"


	message=$(appendJson "${message}" "remotes" "{")

	declare -a remotes=( $(runGit -dir="${srcDir}" remote -v | awk '{print $1"="$2}' | sort -u) )
	declare origin
	declare upstream
	declare remoteUrl
	declare r
	for r in ${remotes[@]}; do
		declare key="${r%%=*}"
		declare value="${r#*=}"

		message=$(appendJson "${message}" "${key}" "${value}" )
		case "${key}" in
		origin)		origin="${value}";;
		upstream)	upstream="${value}";;
		esac

		if [[ "${key}" = "${remote}" ]]; then
			remoteUrl="${value}"
		fi
	done
	message+='}'

	message=$(appendJson "${message}" "remote" "${remote}" )
	message=$(appendJson "${message}" "remoteUrl" "${remoteUrl}" )

	declare branch=$(runGit -dir="${srcDir}" rev-parse --abbrev-ref HEAD)
	message=$(appendJson "${message}" "branch" "${branch}")

	declare repoAvailable
	if verifyGitAvailable -origin="${remote}"; then
		repoAvailable=true
	else
		repoAvailable=false
	fi
	message=$(appendJson "${message}" "repoAvailable" "${repoAvailable}" )

	if ${doFetch}; then
	    if ${repoAvailable}; then
			runGit -dir="${srcDir}" fetch -q --all
			message=$(appendJson "${message}" "fetch" "successful" )
		else
			message=$(appendJson "${message}" "fetch" "repoNotAvailable" )
			[[ -z "${status}" ]] && status="repoNotAvailable"		
		fi
	else
		message=$(appendJson "${message}" "fetch" "notRequested" )
	fi


	declare -a pointers=( $(runGit -dir="${srcDir}" rev-parse HEAD ${branch} ${remote}/${branch} ${mainBranch} ${remote}/${mainBranch} 2> /dev/null) )
	pHead="${pointers[0]}"
	pBranch="${pointers[1]}"
	pRemoteBranch="${pointers[2]}"
	pMain="${pointers[3]}"
	pRemoteMain="${pointers[4]}"


	# Head should point to our branch
	message=$(appendJson "${message}" "pHead" "${pHead}" )
	message=$(appendJson "${message}" "pMain" "${pMain}" )
	message=$(appendJson "${message}" "pBranch" "${pBranch}" )
	message=$(appendJson "${message}" "pRemoteMain" "${pRemoteMain}" )
	message=$(appendJson "${message}" "pRemoteBranch" "${pRemoteBranch}" )

	if	[[ "${pHead}" != "${pBranch}" ]]; then
		message=$(appendJson "${message}" "headCurrent" "false")
		[[ -z "${status}" ]] && status="headNotCurrent"
	else
		message=$(appendJson "${message}" "headCurrent" "true" )
	fi

	# Our local main may need to be pushed to remote
	if [[ "${pMain}" != "${pRemoteMain}" ]]; then
		message=$(appendJson "${message}" "mainCurrent" "false" )
		[[ -z "${status}" ]] && status="mainNotCurrent"
	else
		message=$(appendJson "${message}" "mainCurrent" "true" )
	fi

	# Our local branch may need to be pushed to remote
	if [[ "${pBranch}" != "${pRemoteBranch}" ]]; then
		message=$(appendJson "${message}" "branchCurrent" "false" )
		[[ -z "${status}" ]] && status="branchNotCurrent"
	else
		message=$(appendJson "${message}" "branchCurrent" "true" )
	fi



	# Last commit: see https://git-scm.com/docs/pretty-formats
	latestCommitJson=$(runGit -dir="${srcDir}" log -1 --no-decorate  --pretty=format:'{ "commit":"%H","tree":"%T","parent":"%P","refs":"%D","signature":{"name":"%GS","signerKey":"%GK","fingerPrint":"%GF","verificationFlag":"%G?"},"author":{"name":"%aN","email":"%aE","date":"%ai"},"commiter":{"name":"%cN","email":"%cE","date":"%ci"}')

	declare commitSubject=$(runGit -dir="${srcDir}" log -1 --no-decorate --pretty=format:'%s')
	latestCommitJson=$(appendJson "${latestCommitJson}" "subject" "${commitSubject}")

	declare commitBody=$(runGit -dir="${srcDir}" log -1 --no-decorate --pretty=format:'%B')
	latestCommitJson=$(appendJson "${latestCommitJson}" "commitBody" "${commitBody}")

	declare commitNotes=$(runGit -dir="${srcDir}" log -1 --no-decorate --pretty=format:'%N')
	latestCommitJson=$(appendJson "${latestCommitJson}" "commitNotes" "${commitNotes}")

	declare refs=$(runGit -dir="${srcDir}" show-ref  | grep -v ' refs/tags' | sed 's/ refs\// /'  | awk '{print "\"" $2 "\": \"" $1 "\","}')
	refs=${refs%,}

	message+=", \"refs\": { ${refs} }"

	message+=", \"lastCommit\": ${latestCommitJson} }"

	# Ensure we're clean
	declare data
	data=$(runGit -dir="${srcDir}" status -s)
	if [[ ! -z "${data}" ]]; then
		[[ -z "${status}" ]] && status="notClean"
		clean=false
	fi

	message=$(appendJson "${message}" "clean" "${clean}" )
	message=$(appendJson "${message}" "dirtyFiles" "${data}" )
	message=$(appendJson "${message}" "status" "${status:-ok}" )

	echo "${message}}" | jq -M .

	if ${clean} && ${allCurrent}; then
		return 0
	fi
	return 1
}

####################################################################################################################################
# gitCmd
# --------------------------------------------------------------------------------
# [gitCmdHelp]
# Runs a git command accross multiple git repositories.
#
# .   -h             This help
# .   --debug        Enables debugging
# .   -repo=...      Specifies a source directory to run against (can specify multiple times)
# .   -dir=...       Finds all GIT subdirectories of the specified directory
# .   -dirs=...      Finds all GIT directories under the specified directory
# .   ...            Git command to run in each srcDir
# .   qfetch         Alias for fetch --all --prune 
# .   qpull          Alias for pull --all --prune --ff-only
# .   qgc            Performs an aggressive gc
#
####################################################################################################################################
function gitCmd() {
	declare IFS=$'\n'
	declare -a srcDirs=()
	declare -a cmd=()
	declare cpLogId='gitCmd'
	declare cpLogLevel=${LL_INFO}
	declare verbose=false
	declare mainBranch='main'
	declare -a getInfoOptions=()

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
			-h)
				startSection "gitCmd: Run a git command"
				sectionHeader gitCmdHelp -file="$(show -L gitCmd)"
				endSection
				return 9;;
				
			-v)
				verbose=true;;

			-main=*)
				getInfoOptions+=( "${1}" )
				mainBranch="${1#*=}";;

			--debug)
				cpLogLevel=${LL_DEBUG}
				;;
			-dir=*)
				srcDirs+=( $(find "${1#*=}" -type d -maxdepth 2 -name .git | sed 's/\/\.git$//') )
				;;
			-dirs=*)
				srcDirs+=( $(find "${1#*=}" -type d -name .git | sed 's/\/\.git$//') )
				;;
			-repo=*)
				srcDirs+=( "${1#*=}" )
				;;
			*)
				cmd+=( "${1}" )
				;;
		esac
		shift
	done

	[[ -z "${srcDirs}" ]] && srcDirs=( '.' )

	for srcDir in ${srcDirs[@]}; do
		log -name="gitCmd" debug "Working in ${srcDir}"

		declare projectName="${srcDir##*/}"

		if [[ ! -d "${srcDir}/.git" ]]; then
			log -name="${projectName}" warning "${srcDir} is not a git repository!"
			continue
		fi

		declare gitInfo=$(getGitStatus -dir="${srcDir}" ${getInfoOptions[@]})
		declare branch=$(getJsonValue "${gitInfo}" '.branch')
		declare ourMainBranch=$(getJsonValue "${gitInfo}" '.rootBranch')
		cpLogId="${projectName}/${branch}"

		declare pHead=$(jsonValue "${gitInfo}" '.pHead')
		declare status=$(jsonValue "${gitInfo}" '.status')
       	declare lastCommitUser="$(jsonValue "${gitInfo}" '.lastCommit.author.email')"
		declare lastCommitDate="$(jsonValue "${gitInfo}" '.lastCommit.author.date')"
		declare lastCommitSubj="$(jsonValue "${gitInfo}" '.lastCommit.subject')"

		# Are we readonly or ignored?
		declare mode="$(git -C "${srcDir}" config --local --get user.mode)"
		case "${mode}" in
		'')				;;
		readonly)		readOnly=true; message+=", ${TC_FG_GREEN}readonly${TC_END}";;
		ignored)		continue;;
		*) 				log warning "Unknown repo mode '${mode}'"
		esac

		# Get current information
		declare upstream=$(getJsonValue "${gitInfo}" '.remotes.upstream')
		declare origin=$(getJsonValue "${gitInfo}" '.remotes.origin')

		[[ ! -z "${origin}" ]] && log debug "Origin  : ${origin}"
		[[ ! -z "${upstream}" ]] && log debug "Upstream: ${upstream}"

		declare branchUrl="${origin:-.}/${branch}"

		if ! $(getJsonValue "${gitInfo}" '.repoAvailable'); then
			log info "repo is not available"
			continue
		fi

		if [[ "${status}" != "ok" ]]; then
			startSection "${TC_FG_YELLOW}${projectName}${TC_END} ${TC_FG_WHITE}${branch} @ ${pHead:0:8}, ${lastCommitDate}${TC_END} ${status}"
		else
			startSection "${TC_FG_GREEN}${projectName}${TC_END} ${TC_FG_WHITE}${branch} @ ${pHead:0:8}, ${lastCommitDate}${TC_END}"
		fi

		if ${verbose}; then
			printKeyValueLine "status" "${status}"
			printKeyValueLine "lastCommit" "${lastCommitUser} @ ${lastCommitDate}"
			printKeyValueLine "lastCommitSubj" "${lastCommitSubj}"
			echo ""
		fi

		case "${cmd[*]}" in
			qstatus)
				printKeyValueLine "status" "${status}"
				printKeyValueLine "lastCommit" "${lastCommitUser} @ ${lastCommitDate}"
				printKeyValueLine "lastCommitSubj" "${lastCommitSubj}"
				;;

			url)
				printKeyValueLine "url" "${branchUrl}"
				;;

			qgc)
				git -C "${srcDir}" reflog expire --all --expire=now
				git -C "${srcDir}" fsck --unreachable 
				git -C "${srcDir}" gc --prune=now
				[[ -d "${srcDir}/.git" ]] && find "${srcDir}/.git" -name unhandled.\* -exec rm -f {} \;
				;;

			qfetch)
				git -C "${srcDir}" fetch --all --prune
				;;

			qpull)
				git -C "${srcDir}" pull --all --prune --ff-only
				;;

			*)
				log debug "$(echo "git -C ${srcDir} ${cmd[*]}" | tr '\n' ' ')"
				git -C "${srcDir}" ${cmd[@]}
				;;

		esac
		endSection
		
	done
}

####################################################################################################################################
# gitStatus
# --------------------------------------------------------------------------------
# [gitStatusHelp]
# Status information about git
#
# .   -h             This help
# .   --debug        Enables debugging
# .   -repo=...      Specifies a source directory to run against (can specify multiple times)
# .   -dir=...       Finds all GIT subdirectories of the specified directory
# .   -dirs=...      Finds all GIT directories under the specified directory
# .   ...            Git command to run in each srcDir
#
####################################################################################################################################
function gitStatus() {
	declare IFS=$'\n'
	declare -a srcDirs=()
	declare gitInfo
	declare srcDir
	declare cpLogId='gitStatus'
	declare cpLogLevel=${LL_INFO}

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
			-h)
				startSection "gitStatus: Status about multiple git repos"
				sectionHeader gitStatusHelp -file="$(show -L gitStatus)"
				endSection
				return 9;;
				
			--debug)
				cpLogLevel=${LL_DEBUG}
				;;
			-dir=*)
				srcDirs+=( $(find "${1#*=}" -type d -maxdepth 2 -name .git | sed 's/\/\.git$//') )
				;;
			-dirs=*)
				srcDirs+=( $(find "${1#*=}" -type d -name .git | sed 's/\/\.git$//') )
				;;
			-repo=*)
				srcDirs+=( "${1#*=}" )
				;;
			*)
				cmd+=( "${1}" )
				;;
		esac
		shift
	done

	for srcDir in ${srcDirs[@]}; do
		declare branch
		declare projectName="${srcDir##*/}"

		if [[ ! -d "${srcDir}/.git" ]]; then
			log -name="${projectName}" warning "${srcDir} is not a git repository!"
			continue
		fi

		declare message="${TC_FG_GREEN}fetched${TC_END}"
		declare readOnly=false
		declare ourMainBranch

		declare gitInfo=$(getGitStatus -dir="${srcDir}" ${getInfoOptions[@]})
		declare branch=$(getJsonValue "${gitInfo}" '.branch')
		declare ourMainBranch=$(getJsonValue "${gitInfo}" '.rootBranch')
		cpLogId="${projectName}/${branch}"

		# Are we readonly or ignored?
		declare mode="$(git -C "${srcDir}" config --local --get user.mode)"
		case "${mode}" in
		'')				;;
		readonly)		readOnly=true; message+=", ${TC_FG_GREEN}readonly${TC_END}";;
		ignored)		continue;;
		*) 				log -name="${projectName}" warning "Unknown repo mode '${mode}'"
		esac

		# Get current information
		declare upstream=$(getJsonValue "${gitInfo}" '.remotes.upstream')
		declare origin=$(getJsonValue "${gitInfo}" '.remotes.origin')

		[[ ! -z "${origin}" ]] && log -name="${logKey}" debug "Origin  : ${origin}"
		[[ ! -z "${upstream}" ]] && log -name="${logKey}" debug "Upstream: ${upstream}"

		if ! $(getJsonValue "${gitInfo}" '.repoAvailable'); then
			log info "repo is not available"
			continue
		fi


		declare pBranch=$(getJsonValue "${gitInfo}" ".refs.\"heads/${branch}\"")
		declare pOriginBranch=$(getJsonValue "${gitInfo}" ".refs.\"remotes/origin/${branch}\"")
		declare pUpstreamBranch=$(getJsonValue "${gitInfo}" ".refs.\"remotes/upstream/${branch}\"")

		# Does our branch match origin?
		if [[ ! -z "${pOriginBranch}" ]]; then
			if [[ "${pBranch}" != "${pOriginBranch}" ]]; then
				message+=", ${TC_FG_YELLOW}${branch}!=origin/${branch}${TC_END}"
			fi
		else
			message+=", ${TC_FG_YELLOW}no origin/${branch}${TC_END}"
		fi

		# Does our branch match upstream?
		if [[ ! -z "${pUpstreamBranch}" ]]; then
			if [[ "${pBranch}" != "${pUpstreamBranch}" ]]; then
				message+=", ${TC_FG_YELLOW}${branch}!=upstream/${branch}${TC_END}"
			fi
		elif [[ ! -z "$(getJsonValue "${gitInfo}" ".remotes.upstream")" ]]; then
			message+=", ${TC_FG_YELLOW}no upstream/${branch}${TC_END}"
		fi


		# Ensure we're clean
		if ! $(getJsonValue "${gitInfo}" '.clean'); then
			message+=", ${TC_FG_RED}localDirIsNotClean${TC_END}"
			rc=1
		else
			message+=", ${TC_FG_GREEN}clean${TC_END}"
		fi

		log info "$(trim "${message}")"
	done
}
