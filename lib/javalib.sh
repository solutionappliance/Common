#! /bin/bash
# vim: set ts=4 : set set sw=4 :
####################################################################################################################################
# javalib.sh
####################################################################################################################################
#
# Collection of Java tools
#
# Noralized version numbers:
# v1_8_0_202_oracle: /Library/Java/JavaVirtualMachines/jdk1.8.0_202.jdk
# v1_8_0_222_b10_openjdk: /Library/Java/JavaVirtualMachines/jdk8u222-b10
# v11_0_8_openjdk: /Library/Java/JavaVirtualMachines/jdk-11.0.8.jdk
#
####################################################################################################################################

CP_IMPORTS+="java;"

for srcLib in $(cpLibsToSource json); do
    [[ -e "${srcLib}" ]] && source "${srcLib}" || return 1
done

[[ -z "${CP_JAVA_ENV}" ]] && export CP_JAVA_ENV="${CP_ENV}/$(uname -m)"

#--------------------------------------------------------------------------------
# getJavaHome: Returns a setting for JAVA_HOME
#--------------------------------------------------------------------------------
# Returns the setting for JAVA_HOME using a variety of techniques
#
# 1) Search via JAVA_HOME + java_home + which javac + which java (normal)
# 2) Search via CommonProfile_services_java_version + normal (-cp)
# 3) Using a search parameter (-search=openjdk, -search=1.8, -search=linux)
# 4) Using a specific key (-key=java.mac.v1_8_0_222_b10_openjdk)
#--------------------------------------------------------------------------------

function getJavaHome() {
	declare IFS=$'\n'
	declare mac=true
	declare verbose=false
	declare dir=''
	declare cmd="find"
	declare -a search=()

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-v) 					verbose=true; search+=( -v );;
		-nomac)					mac=false;;
		-cp)					cmd="profile";;
		-search=*)				search+=( "${1}" ); cmd='search';;
		-key=*)					cmd="key=${1#*=}";;
		-dir=*)					path="${1#*=}"; cmd='find';;
		*)						log -name="getJavaHome" error "Unsupported option ${1}"; return 9;;
		esac
		shift
	done

	declare json
	if ! json="$(cat "${CP_JAVA_CONFIG}")"; then
		log -name=getJavaHome error "Cannot find config: ${CP_JAVA_CONFIG}"
		return 2
	fi

	declare value


	#
	# Find by CommonProfile_services_java_version
	#
	if [[ "${cmd}" = 'profile' ]]; then
		if [[ ! -z "${CommonProfile_services_java_version}" ]]; then
			search+=( "-search=${CommonProfile_services_java_version}" )
			${verbose} && log -name=getJavaHome info "Searching for CommonProfile_services_java_version=${CommonProfile_services_java_version}"
			for result in $(searchJavaKey "${search[@]}" -m); do
				if value="$(getJsonValue "${json}" ".${result}.root")"; then
					[[ -d "${value}" ]] && dir="${value}" || value=''
					${verbose} && log -name=getJavaHome info "Found ${result} by CommonProfile_services_java_version (dir=${value:-notFound})"
				fi
			done
		fi

		[[ -z "${dir}" ]] && cmd="find"
		cmd='find'
		dir=''
	fi

	case "${cmd}" in
		none)
			;;

		#
		# Find
		#
		find)
			[[ -z "${dir}" ]] && dir="${JAVA_HOME}"

			if [[ ! -z "${dir}" ]]; then
				${verbose} && log -name=getJavaHome info "Found ${dir} via JAVA_HOME}"
			elif [[ -e /usr/libexec/java_home ]] && dir="$(/usr/libexec/java_home)"; then
				${verbose} && log -name=getJavaHome info "Found ${dir} via /usr/libexec/java_home"
			elif dir="$(command -v javac 2> /dev/null)"; then
				${verbose} && log -name=getJavaHome info "Found ${dir} via command -v javac"
			elif dir="$(command -v java 2> /dev/null)"; then
				${verbose} && log -name=getJavaHome info "Found ${dir} via command -v java"
			else
				log error "JAVA_HOME not set no javac, and no java"
				return 1
			fi
		;;

		key=*)
			if value="$(getJsonValue "${json}" ".${cmd#*=}.root")"; then
				[[ -d "${value}" ]] && dir="${value}" || value=''
				${verbose} && log -name=getJavaHome info "Found ${cmd#*=} (dir=${value:-notFound})"
			fi
		;;

		#
		# Search
		#
		search)
			for result in $(searchJavaKey "${search[@]}" -m); do
				if value="$(getJsonValue "${json}" ".${result}.root")"; then
					[[ -d "${value}" ]] && dir="${value}" || value=''
					${verbose} && log -name=getJavaHome info "Found ${result} (dir=${value:-notFound})"
				fi
			done

			;;

		*)
			log -name="getJavaHome" error "Unsupport command ${cmd}"
			return 1;
		;;
	esac

	if [[ -z "${dir}" ]]; then
		${verbose} && log -name=getJavaHome info "Specified java version not found"
		return 1
	fi

	${verbose} && log -name=getJavaHome info "Found ${dir}"
	while [[ ! -z "${dir}" ]] && [[ -L "${dir}" ]]; do
		dir="$(readlink "${dir}")"
		${verbose} && log -name=getJavaHome info "Followed link to ${dir}"
	done

	[[ -f "${dir}" ]] && dir="$(dirname "${dir}")"

	if [[ "${dir}" = *"/Contents/Home" ]]; then
		dir="${dir%/*}"
		dir="${dir%/*}"
	elif [[ "${dir}" = *"/bin" ]]; then
		dir="${dir%/*}"
	fi

	if ${mac} && [[ -x "${dir}/Contents/Home/bin/java" ]]; then
		dir+="/Contents/Home"
	fi

	echo "${dir}"
}

#--------------------------------------------------------------------------------
# searchJavaKey: Finds a Java configuration key
#--------------------------------------------------------------------------------
# Uses search criteria to find a java key.
#
# -search=[0-9]*    Search by version number
# -search=openjdk   Search by vendor
# -search=mac       Search by environment (defaults to CP_JAVA_ENV)
#--------------------------------------------------------------------------------

function searchJavaKey() {
	declare IFS=$'\n'
	declare verbose=false
	declare searchVendor=''
	declare searchVersion=''
	declare env="${CP_JAVA_ENV//[-\/]/_}"
	declare found=false
	declare multi=false

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-v) 							verbose=true;;
		-m)								multi=true;;
		-search=[0-9]*)					searchVersion="${1#*=}";;
		-search=oracle|-search=openjdk|-search=graalvm) 
			searchVendor="${1#*=}";;
		-env=*)							env="${1#*=}";;
		-search=mac|-search=linux)		env="${1#*=}";;
		-search=none) 					env="";;
		*)
			log -name="searchJavaKey" error "Unsupported option ${1}"; return 9;;
		esac
		shift
	done

	declare json
	declare environments

	declare search="^java\\."
	[[ ! -z "${env}" ]] && search+="${env//-/_}\\." || search+="[^0-9]*\\."
	[[ ! -z "${searchVersion}" ]] && search+="v${searchVersion//\./_}"
	search+=".*"
	[[ ! -z "${searchVendor}" ]] && search+="_${searchVendor//\./_}"
	search+="\\.root$"

	if ! json="$(cat "${CP_JAVA_CONFIG}")"; then
		log -name=findJavaKey error "Cannot find config: ${CP_JAVA_CONFIG}"
		return 2
	fi

	
	${verbose} && log -name=searchJavaKey info "Searching for ${search}"
	declare results="$(getJsonValue -l "${json}" | ${grep} -E -e "${search}" | sed -e 's/\.root$//' )"

	if [[ ! -z "${results}" ]]; then
		${multi} && echo "${results}" || echo "${results}" | tail -1
		return 0
	else
		${verbose} && log -name=searchJavaKey info "Search for ${search} failed"
	fi
}

#--------------------------------------------------------------------------------
# parseJavaKey: Returns a key for a directory name
#--------------------------------------------------------------------------------
# Returns the best guess at the key for the specified JAVA_HOME directory.
#--------------------------------------------------------------------------------

function parseJavaKey() {
	declare verbose=false
	declare env="${CP_JAVA_ENV//[-\/]/_}"
	declare dir

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-env=*)				env="${1#*=}";;
		-v)					verbose=true;;
		-dir=*)				dir="${1#*=}";;
		-env=*)				env="${1#*=}";;
		*)	
			if [[ -z "${dir}" ]] && [[ "${1}" != -* ]]; then
				dir="${1}"
			else
				log -name="parseJavaVersion" error "Unknown option ${1}"
				return 9
			fi
			;;
		esac
		shift 
	done

	[[ -z "${dir}" ]] && dir="$(getJavaHome -nomac)"

	if [[ -z "${dir}" ]]; then
		log -name="parseJavaVersion" error "No java directory specified"
		return 1
	fi

	${verbose} && log -name="parseJavaVersion" info "Parsing ${dir}"

	declare major
	declare minor
	declare release
	declare vendor

	if [[ "${dir}" =~ (.*/)?jdk([0-9]*)u([0-9.]*)(-(b[0-9]*))?(/.*)? ]]; then
		major="${BASH_REMATCH[2]}"
		minor="${BASH_REMATCH[3]}"
		release="${BASH_REMATCH[5]}"

		echo "java.${env}.v1_${major}_0_${minor}${release:+_${release}}_openjdk"
		return 0

	# v1_8_0_192_oracle: "/Library/Java/JavaVirtualMachines/jdk1.8.0_192.jdk
	elif [[ "${dir}" =~ (.*/)?jdk([0-9.]*)(_([0-9]*))\.jdk(/.*)? ]]; then
		major="${BASH_REMATCH[2]//\./_}"
		release="${BASH_REMATCH[4]}"

		echo "java.${env}.v${major}${release:+_${release}}_oracle"
		return 0

	# "v11_0_8_openjdk": "root": "/Library/Java/JavaVirtualMachines/jdk-11.0.8.jdk"
	elif [[ "${dir}" =~ (.*/)?jdk-([0-9.]*)\.jdk(/.*)? ]]; then
		major="${BASH_REMATCH[2]//\./_}"
		echo "java.${env}.v${major}_openjdk"

		return 0


	# /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.302.b08-0.el7_9.x86_64/jre/bin/java
	elif [[ "${dir}" =~ (.*/.*)?openjdk-([0-9.]*)\.(b[0-9]*)?(.*)? ]]; then
		major="${BASH_REMATCH[2]//\./_}"
		release="${BASH_REMATCH[3]}"

		echo "java.${env}.v${major}${release:+_${release}}_openjdk"
		return 0

	elif [[ "${dir}" =~ (.*/.*)?openjdk-([0-9.]*)(.*)? ]]; then
		major="${BASH_REMATCH[2]//\./_}"

		echo "java.${env}.v${major}}_openjdk"
		return 0
	fi


	${verbose} && log -name="parseJavaVersion" error "Cannot parse ${dir}"
	return 1
}

function getJavaVersionFromKey() {
	declare key="${1}"

	if [[ -z "${key}" ]]; then
		log -name="getJavaVersionFromKey" error "No key specified"
		return 1
	fi

	key="${key%.root}"
	key=${key##*.}
	key=${key#*v}
	key=${key//_/.}

	vendor=${key##*.}
	key="${key%.*}"
	
	echo "${vendor} ${key}"
}

function lsJava() {
	declare environments
	declare json="$(cat "${CP_JAVA_CONFIG}")"
	declare env="${CP_JAVA_ENV//[-\/]/_}"

	if ! environments="$(getJsonValue -l "${json}")"; then
		log -name=lsJava error "Cannot find config: ${CP_JAVA_CONFIG}"
		return 2
	fi


	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-env=*)
			env="${1#*=}";;
		*)	
			log -name="lsJava" error "Unknown option ${1}"
			return 9;;
		esac
		shift 
	done

	declare search="^java.${env}..*.root$"
	declare key
	for key in $(echo "${environments}" | grep -E -e "${search}"); do
		declare version="$(getJavaVersionFromKey ${key})"

		declare value="$(getJsonValue "${json}" ".${key}")"
		if [[ -d "${value}/Contents/Home" ]]; then
			value+='/Contents/Home'

			printKeyValueLine "${version}" "${value}"
		elif [[ -d "${value}" ]]; then
			value+='/Contents/Home'

			printKeyValueLine "${version}" "${value}"
		else
			value="$(getJsonValue "${json}" ".${key%.root}.download")"
			printKeyValueLine "${version}" "${TC_FG_WHITE}installable from ${value}${TC_END}"
		fi
	done
}

function whichJava() {
	declare verbose=false
	declare environments
	declare javaHome=''
	declare showKey=false
	declare -a opts=()

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
			-env=*)	opts+=( -env="${1#*=}" );;
			-v)		verbose=true;;
			*)	if [[ -z "${javaHome}" ]] && [[ "${1}" != -* ]]; then
					javaHome="${1}"
				else
					log -name=whichJava error "Unsupport option ${1}"
					return 9;
				fi
				;;
		esac
		shift
	done

	if [[ -z "${javaHome}" ]]; then
		if ! javaHome="$(getJavaHome -nomac)"; then
			log -name=whichJava error "Cannot find JAVA_HOME"
			return 1
		fi
	fi

	${verbose} && log -name=whichJava info "javaHome is ${javaHome}"
	
	if parseJavaKey ${opts[@]} "${javaHome}"; then
		return 0
	fi

	return 1
}

#--------------------------------------------------------------------------------
# setJava version
#--------------------------------------------------------------------------------
# Set JAVA_HOME to the specified version
#
function setJava() {
	declare -a getOptions=()
	declare verbose=false
	declare doWork=true
	declare oldJava="${JAVA_HOME}"

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-v)			verbose=true; getOptions+=( -v );;
		-cp)		getOptions+=( -cp );;
		-nomac)		getOptions+=( -nomac );;
		-search=*)	getOptions+=( "${1}" );;
		-key=*)		getOptions+=( "${1}" );;
		-dir=*)		getOptions+=( "${1}" );;
		-n)			doWork=false;;
		[0-9]*|graalvm|oracle|openjdk|mac|linux)
					getOptions+=( "-search=${1}" )
					;;
		*)			log -name="setJava" error "Unsuported option ${1}"; return 9;;
		esac
		shift
	done


	declare javaHome=$(getJavaHome "${getOptions[@]}")

	if [[ ! -z "${javaHome}" ]]; then
		declare oldJavaHome="${JAVA_HOME}"
		${verbose} && log info "export JAVA_HOME=\"${javaHome}\""
		${doWork} && export JAVA_HOME="${javaHome}"

		declare cppFlags="${CPPFLAGS}"
		[[ " ${cppFlags} " != *" -I ${javaHome}/include " ]] && cppFlags="${cppFlags:+${cppFlags} }-I ${javaHome}/include"
		[[ "${javaHome}" = *"/Contents/Home" ]] && [[ " ${cppFlags} " != *" -I ${javaHome}/include/darwin " ]] && cppFlags="${cppFlags:+${cppFlags} }-I ${javaHome}/include/darwin"

		${verbose} && log info "export CPPFLAGS=\"${cppFlags}\""
		${doWork} && export CPPFLAGS="${cppFlags}"

		${verbose} && log info "export PATH=\"${javaHome}/bin:${PATH}\""
		if ${doWork}; then
			if [[ ! -z "${oldJavaHome}" ]] && [[ "${oldJavaHome}" != "${javaHome}" ]]; then
				export PATH="${PATH/${oldJavaHome}\/bin:/}"
			fi

			addToPathIfNeeded -front -sep=':' PATH "${javaHome}/bin"
		fi

		return 0
	fi

	log -name=setJava error "Cannot find the jdk"
	return 1
}

function initJava() {
	if CP_JAVA_CONFIG=$(resolvePath "${CP_ETC_PATH}" javaconfig.json); then
		export CP_JAVA_CONFIG
	fi
}

function installJava() {
	declare javaKey=''
	declare verbose=false
	declare -a search=()
	declare json
	declare doDownload=false
	declare doWork=true
	declare mac=false

	[[ "${CP_ENV}/" = "mac/"* ]] && mac=true

	while [[ ! -z  "${1}" ]]; do
		case "${1}" in
		-d)	doDownload=true;;
		-n)	doWork=false;;
		-v)	verbose=true;;
		-nomac)	mac=false;;
		-mac)	mac=true;;
		-cp)	[[ ! -z "${CommonProfile_services_java_version}" ]] && search+=( "-search=${CommonProfile_services_java_version}" );; 
		-search=*) search+=( "${1}" ) ;;
		[0-9]*|oracle|openjdk|graalvm|mac|linux)
			search+=( "-search=${1}" )
			;;
		*)	log -name=installJava error "Unknown option ${1}"
			return 9;;
		esac
		shift
	done

	[[ -z "${search}" ]] && [[ ! -z "${CommonProfile_services_java_version}" ]] && search+=( "-search=${CommonProfile_services_java_version}" )
	${verbose} && search+=( -v )

	if ! json="$(cat "${CP_JAVA_CONFIG}")"; then
		log -name=findJavaKey error "Cannot find config: ${CP_JAVA_CONFIG}"
		return 2
	fi

	if [[ -z "${javaKey}" ]]; then
		if ! javaKey=$(searchJavaKey "${search[@]}" ); then
			log -name=installJava error "Cannot find key for ${search[@]}: ${javaKey}"
			return 2
		fi
	fi

	${verbose} && log -name=installJava info "Found ${javaKey}"

	declare javaPolicyNeeded=false
	declare javaRootDir="$(getJsonValue "${json}" ".${javaKey}.root")"
	declare javaHomeDir="${javaRootDir}"
	${mac} && javaHomeDir+="/Contents/Home"
	[[ "${javaKey}" = *".v1_8_"* ]] && javaPolicyNeeded=true

	if [[ -d "${javaRootDir}" ]]; then
		log -name=installJava info "${javaRootDir} already installed"
		return 0
	fi

	declare javaSourceUrl="$(getJsonValue "${json}" ".${javaKey%.root}.download")"
	declare javaSourceFile

	if [[ "${javaSourceUrl}" = yum:* ]]; then
		javaSourceFile="${javaSourceUrl}"
	else
		if ! javaSourceFile="$(findInstallable "${javaSourceUrl}")"; then
			log -name=installJava error "Cannot locate javaSourceFile ${javaSourceUrl}"
			${doWork} && return 1
		fi
	fi

	declare javaPolicySourceFile=''
	if ${javaPolicyNeeded}; then
		declare javaPolicySourcePath="$(getJsonValue "${json}" ".java.policy.download")"
		if ! javaPolicySourceFile="$(findInstallable "${javaPolicySourcePath}")"; then
			log -name=installJava error "Cannot locate javaPolicySourceFile ${javaPolicySourcePath}"
			${doWork} && return 1
		fi

		${verbose} && log -name=installJava info "javaPolicySourcePath: ${javaPolicySourcePath}"
		${verbose} && log -name=installJava info "javaPolicySourceFile: ${javaPolicySourceFile}"
	fi

	log -name=installJava info "Installing ${javaKey}, source=${javaSourceFile}, policy=${javaPolicySourceFile:-notNeeded}, dest=${javaRootDir}"

	case "${javaSourceFile}" in
	yum:*)
		log -name=java info "yum install -y $(echo "${javaSourceFile#*:}" | tr ';' ' ')"
		${doWork} && sudo yum install -y $(echo "${javaSourceFile#*:}" | tr ';' '\n')
		;;
	*.tar.gz)
		declare dir="$(dirname "${javaRootDir}")"
		log -name=java info "sudo tar -C "${dir}/" -xzf \"${javaSourceFile}\""
		${doWork} && sudo tar -C "${dir}/" -xzf "${javaSourceFile}"
		;;
	*.dmg)
		log -name=java info "Process ${javaSourceFile}"
		if ${doWork}; then
			declare javaAttachment=$(hdiutil attach -noverify -readonly -noautoopen "${javaSourceFile}")
			declare javaDmgDev=$(echo "${javaAttachment}" | head -1 | awk '{print $1}')
			declare javaDmgDir=$(echo "${javaAttachment}" | tail -1 | cut -c54-)

			declare package=$(find "${javaDmgDir}" -name \*.pkg -depth 1 2> /dev/null | head -1)
			sudo installer -pkg "${package}" -target /
			hdiutil detach "${javaDmgDev}"
		fi
		;;
	*)
		log -name=installJava info "Do not know how to handle ${javaSourceFile}"
		return 1
	esac

	if ${javaPolicyNeeded}; then
		log -name=java info "Install ${javaPolicySourceFile} to ${javaHomeDir}/jre/lib/security"
		if ${doWork}; then
			sudo rm -f "${javaHomeDir}/jre/lib/security/local_policy.jar" "${javaHomeDir}/jre/lib/security/US_export_policy.jar"
			sudo unzip -j "${javaPolicySourceFile}" "UnlimitedJCEPolicyJDK8/local_policy.jar" -d "${javaHomeDir}/jre/lib/security"
			sudo unzip -j "${javaPolicySourceFile}" "UnlimitedJCEPolicyJDK8/US_export_policy.jar" -d "${javaHomeDir}/jre/lib/security"
		fi
	else
		log -name=installJava info "Java policy not needed"
	fi

	log -name=installJava info "setJava -key=${javaKey}"
	${doWork} && setJava -key="${javaKey}"

	return 0
}

initJava
