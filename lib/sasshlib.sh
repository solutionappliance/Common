#! /bin/bash
# vim:sw=4:ts=4:softtabstop=4:noexpandtab:
####################################################################################################################################
# Solution Appliance SSH replacement
####################################################################################################################################
#
#
####################################################################################################################################

CP_IMPORTS+="sassh;"

function sassh() {
	if [[ "${1}" == '-l' ]] || [[ -z "${1}" ]]; then
		grep "^Host " ~/.ssh/config ~/.ssh/*.cfg 2> /dev/null | awk '{print $2}' | grep -v '^*' | sort
		return 0
	fi

	setTerminalTabName "${@}"
	ssh "${@}"
	setTerminalTabName "Default"
}

function _completeSaSsh() {
	export IFS=$'\n'
	declare -a projects=( $(grep "^Host " ~/.ssh/config ~/.ssh/*.cfg 2> /dev/null | awk '{print $2}' | grep -v '^*' | sort) )
	projects+=( $(cat /etc/hosts | sed -E -e '/#.*/s///' -e '/^$/d' -e '/^[0-9:.]*[[:space:]]*/s///' -e '/[[:space:]]*$/s///' | tr ' ' '\n' | sort -u ) )
	COMPREPLY=( $(compgen -W '${projects[@]}' -- ${COMP_WORDS[COMP_CWORD]}) )
}

rename_function _known_hosts_real _known_hosts_real_bak &> /dev/null

function _known_hosts_real() {
	export IFS=$'\n'
	declare -a projects=( $(${grep:-grep} "^Host " ~/.ssh/config ~/.ssh/*.cfg 2> /dev/null | awk '{print $2}' | grep -v '^*' | sort) )
	projects+=( $(cat /etc/hosts | sed -E -e '/#.*/s///' -e '/^$/d' -e '/^[0-9:.]*[[:space:]]*/s///' -e '/[[:space:]]*$/s///' | tr ' ' '\n' | sort -u ) )
	COMPREPLY=( $(compgen -W '${projects[@]}' -- ${COMP_WORDS[COMP_CWORD]}) )
}

complete -F _completeSaSsh sassh

