# /bin/bash
# vim:ts=4:sw=4:nowrap:
####################################################################################################################################
# projectlib.sh
####################################################################################################################################
#
# project() and projects() scripts
#
####################################################################################################################################

CP_IMPORTS+="project;"

function getProjectVariable() {
	declare data="${1}"
	declare key="${2}"

	if [[ $'\n'"${data}"$'\n' =~ $'\n'"${key}:"[[:space:]]*([^$'\n']*)$'\n' ]]; then
		echo "${BASH_REMATCH[1]}"
		return 0
	fi

	return 1
}

function readProjectData() {
	declare file="${1}"

	if [[ ! -r "${file}" ]]; then
		log info "Cannot read ${file}"
		return 1
	fi

	declare data=$("${sed}" -E -e '1,/^####/ d ; /^###/,$ d ; s/^#?\s*// ; /^$/d' "${file}")
#	if [[ $'\n'"${data}"$'\n' =~ $'\n'"Project" ]]; then
	if [[ $'\n'"${data}"$'\n' =~ $'\n'[a-zA-Z] ]]; then
		echo "${data}"
		return 0
	fi

	return 1
}

function getProjectList() {
	declare IFS=';'
	declare projectPath

	for projectPath in ${CP_PROJECT_PATH}; do
		IFS=$'\n'
		for projectFile in $(ls -d "${projectPath}"/*/.profile.sh 2> /dev/null); do
			declare dataBlock projectDir projectId
			if dataBlock=$(readProjectData "${projectFile}"); then
				declare projectDir="${projectFile%/*}"
				projectId="${projectDir##*/}"
				projectTitle=$(getProjectVariable "${dataBlock}" 'Project.title') || projectTitle="${projectId}"

				if [[ "$(getProjectVariable "${dataBlock}" 'Project.active')" != "false" ]]; then
					echo "${projectDir}|${projectId}|${projectTitle}"
				fi
			fi
		done
	done
}

function listProjects() {
	declare IFS=$'\n'
	declare entry

	printf "${TC_FG_YELLOW}%s${TC_END}\n" "${CP_PROJECT_ACTIVE_ID_PATH:-/}"

	for entry in $(getProjectList); do
		declare IFS='|'
		declare -a projectData=( ${entry} )
		printf "${TC_FG_BLUE}%-20s${TC_END}: ${TC_FG_WHITE}%s${TC_END}\n" "${projectData[1]}" "${projectData[2]}";
	done;
	[[ -z "${entry}" ]] && return 1 || return 0
}

function _completeProjectList() {
	declare IFS=$'\n'
	declare projects=( $(getProjectList | cut -f2 -d'|') )
	COMPREPLY=( $(compgen -W '${projects[@]}' -- ${COMP_WORDS[COMP_CWORD]}) )
}

function sourceProjectPath() {
	declare quiet=true
	declare -a sourceOpts=()
	declare projectsPath

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-m)	quiet=false; 
			sourceOpts+=( "-m" )
			;;
		-*)
			log error "sourceProjectPath: Unsupported option ${1}"
			return 9
			;;
		*)
			[[ ! -z "${projectPath}" ]] && { log error "project: Cannot specify multiple projects!" ; return 2; }
			projectsPath="${1}"
		esac
		shift
	done

	while [[ "${projectsPath}" =~ ^/?([^/]+)(.*)$ ]]; do
		declare projectFile="${BASH_REMATCH[1]}"
		projectsPath="${BASH_REMATCH[2]}"

		declare fqProjectFile=$(resolvePath "${CP_PROJECT_PATH}" "${projectFile}/.profile.sh")
		if [[ -z "${fqProjectFile}" ]]; then
			log error "Unable to locate ${projectFile}/.profile.sh in \"${CP_PROJECT_PATH}\""
			return 1
		fi
		sourceProjectFile "${sourceOpts[@]}" "${fqProjectFile}" || return 2
	done

	export CP_SETTINGS="{\"active\":{\"type\":\"top\",\"id\":\"${CP_PROJECT_ACTIVE_ID_PATH}\"}}"
}

function sourceProjectFile() {
	declare quiet=true
	declare fqProjectFile

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-m)	quiet=false;;
		-*)
			log error "sourceProjectFile: Unsupported option ${1}"
			return 9
			;;
		*) 	fqProjectFile="${1}";;
		esac
		shift
	done

	if [[ -z "${fqProjectFile}" ]]; then
		return 0
	fi

	if [[ ! -r "${fqProjectFile}" ]]; then
		log error "project: Cannot locate project ${fqProjectFile}"
		return 2
	fi

	declare IFS='/'
	declare projectDir projectId projectTitle dataBlock
	projectDir="${fqProjectFile%/*}"

	if ! dataBlock=$(readProjectData "${fqProjectFile}"); then
		log error "project: Project ${fqProjectFile} is invalid"
		return 1
	fi

	projectId="${projectDir##*/}"
	projectTitle=$(getProjectVariable "${dataBlock}" 'Project.title') || projectTitle="${projectId}"

	export CP_PROJECT_ACTIVE_ID="${projectId}"
	export CP_PROJECT_ACTIVE_DIR="${projectDir}"
	export CP_PROJECT_ACTIVE_TITLE="${projectTitle}"

	if [[ "${CP_PROJECT_ACTIVE_ID_PATH}/" != *"/${projectId}/"* ]]; then
		export CP_PROJECT_ACTIVE_ID_PATH="${CP_PROJECT_ACTIVE_ID_PATH}/${projectId}"
	fi

	# Env settings
	declare IFS=$'\n'
	unset CP_PROJECT_PATH
	declare etcDir libDir tDir binDir svcDir prjDir
	declare env=${CP_ENVS:-$(getCommonProfileSetting env)}
	declare -a envs=( "." ${env//[,;]/$'\n'} )

	for id in ${envs[@]}; do
		if [[ -z "${id}" ]] || [[ "${id}" = '.' ]]; then
			etcDir="${projectDir}/etc"
			libDir="${projectDir}/lib"
			tDir="${projectDir}/template"
			binDir="${projectDir}/bin"
			svcDir="${projectDir}/services"
		else
			etcDir="${projectDir}/etc/${id}"
			libDir="${projectDir}/lib/${id}"
			tDir="${projectDir}/template/${id}"
			binDir="${projectDir}/bin/${id}"
			svcDir="${projectDir}/services/${id}"
		fi

		[[ -d "${etcDir}" ]] && [[ ";${CP_ETC_PATH};" != *";${etcDir};"* ]] && export CP_ETC_PATH="${etcDir};${CP_ETC_PATH}"
		[[ -d "${libDir}" ]] && [[ ";${CP_LIB_PATH};" != *";${libDir};"* ]] && export CP_LIB_PATH="${libDir};${CP_LIB_PATH}"
		[[ -d "${tDir}" ]] && [[ ";${CP_TEMPLATE_PATH};" != *";${tDir};"* ]] && export CP_TEMPLATE_PATH="${tDir};${CP_TEMPLATE_PATH}"
		[[ -d "${binDir}" ]] && [[ ":${PATH}:" != *":${binDir}:"* ]] && export PATH="${binDir}:${PATH}"
		[[ -d "${svcDir}" ]] && [[ ";${CP_SERVICE_PATH};" != *";${svcDir};"* ]] && export CP_SERVICE_PATH="${svcDir};${CP_SERVICE_PATH}"
		[[ -d "${prjDir}" ]] && CP_PROJECT_PATH="${prjDir}"

	done

	# Source project profile
	if [[ -z "${projectsPath}" ]] && ! ${quiet}; then
		cd "${projectDir}"
		source "${fqProjectFile}"
	else
		source "${fqProjectFile}" -q
	fi
}

function toProject() {
	declare IFS=$'\n'
	declare quiet=false
	declare projectFile
	declare runShell=true
	declare interactive=false

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-h)
			printfln "go to and load a project"
			printKeyValueLine "-h" "Display help"
			printKeyValueLine "-l" "List projects"
			printKeyValueLine "-i" "Interactive mode"
			printKeyValueLine "-" "Unload project"
			printKeyValueLine "-q" "Quiet true, run shell true"
			printKeyValueLine "-s" "Quiet true, run shell false"
			printKeyValueLine "-b" "Quiet false, run shell false"
			return 0
			;;
		-q)	quiet=true;;
		-l)	listProjects "${@}"; return;;
		-s)	runShell=false; quiet=true;;
		-b)	runShell=false;;
		-i)	interactive=true;;
		-)	if [[ ! -z "${CP_PROJECT_ACTIVE_ID_PATH}" ]]; then
				if ${runShell}; then
					exit 0 &> /dev/null
				else
					export CP_PROJECT_ACTIVE_ID_PATH="${CP_PROJECT_ACTIVE_ID_PATH%/*}"
					projectFile="${CP_PROJECT_ACTIVE_ID_PATH##*/}"

					if [[ -z "${projectFile}" ]]; then
						unset CP_PROJECT_ACTIVE_ID
						unset CP_PROJECT_ACTIVE_TITLE
						unset CP_PROJECT_ACTIVE_DIR
						unset CP_PROJECT_ACTIVE_ID_PATH
						export CP_PROJECT_ACTIVE_ID CP_PROJECT_ACTIVE_TITLE CP_PROJECT_ACTIVE_DIR CP_PROJECT_ACTIVE_ID_PATH
						return 0
					fi
				fi
			fi
			;;
		-*)
			log error "toProject: Unsupported option ${1}"
			return 9
			;;
		*)
			[[ ! -z "${projectFile}" ]] && { log error "toProject: Cannot specify multiple projects!" ; return 2; }
			projectFile="${1}"
		esac
		shift
	done

	if [[ -z "${projectFile}" ]]; then
		if [[ -z "${CP_PROJECT_ACTIVE_DIR}" ]]; then
			log error "toProject: Project name or directory must be specified (-h for help)"
			return 1
		fi
		projectFile="${CP_PROJECT_ACTIVE_DIR}"
	fi

	declare fqProjectFile
	if [[ -r "${projectFile}/.profile.sh" ]]; then
		fqProjectFile=$(fullyQualifiedPath "${projectFile}/.profile.sh")
	else
		fqProjectFile=$(resolvePath "${CP_PROJECT_PATH}" "${projectFile}/.profile.sh")
	fi

	if [[ ! -r "${fqProjectFile}" ]]; then
		log error "toProject: Cannot locate project ${projectFile}"
		return 1
	fi

	declare projectDir projectId projectTitle dataBlock
	projectDir="${fqProjectFile%/*}"
	projectId="${projectDir##*/}"
	[[ "${projectDir}" == "${CP_PROJECT_ACTIVE_DIR}" ]] && runShell=false


	if ${runShell}; then
		CP_SETTINGS="{\"active\":{\"type\":\"top\",\"id\":\"-m ${CP_PROJECT_ACTIVE_ID_PATH}/${projectId}\"}}" bash
		[[ "${CP_PROJECT_ACTIVE_DIR}" ]] && toProject
		return 0
	fi

	if [[ "${CP_PROJECT_ACTIVE_ID_PATH}/" != *"/${projectId}/"* ]]; then
		export CP_PROJECT_ACTIVE_ID_PATH="${CP_PROJECT_ACTIVE_ID_PATH}/${projectId}"
	fi

	if ! ${quiet}; then
		cd "${projectDir}"
		sourceProjectFile -m "${fqProjectFile}"
	else
		sourceProjectFile "${fqProjectFile}"
	fi
}

complete -F _completeProjectList toProject

copy_function toProject to

complete -F _completeProjectList to

# interactive function that mimics go and to interactive functionality
function goto() {
  println "shortcuts:"
  declare IFS=$'\n'
  declare -a goList=$(getGoList | sort)
  declare gotoNum=0
  for goEntry in ${goList}; do
    declare goKey="${goEntry%=*}"
    [[ "${goKey}" == "POP" ]] && continue
    declare goValue="${goEntry#*=}"
    let gotoNum++
    if [[ "${goValue}" = *'${'* ]]; then
      goValue=$(eval "echo \"${goValue}\"")
    fi
    printKeyValueLine "${gotoNum}: ${goKey}" "${goValue}"
  done

  println "projects:"
  declare IFS=$'\n'
  for entry in $(getProjectList); do
    let gotoNum++
    declare IFS='|'
    declare -a entryA=( ${entry} )
    declare proPath="${entryA[0]}"
    declare proName="${entryA[1]}"
    printKeyValueLine "${gotoNum}: ${proName}" "${proPath}"
  done

  echo -n "${TC_FG_GREEN}Enter number or press [return] to exit: ${TC_END}"
  read response
  if [[ -z "${response}" ]]; then
    return 1
  fi
  if [[ ! "${response}" =~ ^-?[0-9]+$ ]]; then
    return 1
  fi

  gotoNum=0
  declare IFS=$'\n'
  for goEntry in ${goList}; do
    declare goKey="${goEntry%=*}"
    declare goValue="${goEntry#*=}"
    [[ "${goKey}" == "POP" ]] && continue
    let gotoNum++
    if [[ "${response}" == "${gotoNum}" ]]; then
      go ${goKey}
      return 0
    fi
  done

  declare IFS=$'\n'
  for entry in $(getProjectList); do
    let gotoNum++
    if [[ "${response}" == "${gotoNum}" ]]; then
      declare IFS='|'
      declare -a entryA=( ${entry} )
      projectFile="${entryA[1]}"
      toProject "${projectFile}"
      return 0
    fi
  done

  echo "${response} not found."
  return 1
}
