# /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# setupenv.sh
####################################################################################################################################
#
# Adds Common Profile configuration to the standard environment.
#
####################################################################################################################################

CP_IMPORTS+="cp/setupenv;"

if [[ -z "${CP_RSIE:-}" ]] && [[ "${1}" != '-init' ]]; then
	export CP_RSIE=true
fi


#
# Additional libraries to source
#
declare srcLib
for srcLib in $(cpLibsToSource json service project go); do
	source "${srcLib}" || exit 1
done

# Config "go" to go home
[[ -z "${CP_GO_root}" ]] && export CP_GO_root="${HOME}"

# Add Projects directory to CP_PROJECT_PATH if it is not already there
[[ -d "${HOME}/Projects" ]] && addToPathIfNeeded CP_PROJECT_PATH "${HOME}/Projects"

# Find HOMEBREW
if [[ -z "${HOMEBREW_PREFIX}" ]]; then
	if [[ -x /usr/local/bin/brew ]]; then
		export HOMEBREW_PREFIX=/usr/local
	elif [[ -x /opt/homebrew/bin/brew ]]; then
		export HOMEBREW_PREFIX=/opt/homebrew
	fi
fi

# CP_REMOTE_SYSNAME
if [[ -z "${CP_REMOTE_SYSNAME}" && ! -z "${SSH_CLIENT}" ]]; then
	export CP_REMOTE_SYSNAME="$(echo "${SSH_CLIENT}" | awk '{print $1}')"
fi
