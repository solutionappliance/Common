#! /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# eclipselib.sh
####################################################################################################################################
#
# Common functions for Eclipse
#
####################################################################################################################################

function initEclipse() {
	declare reset=false
	declare verbose=false

	while [[ ! -z "${1}"  ]]; do
		case "${1}" in
		-v)	verbose=true;;
		-r)	reset=true;;
		*)	log -name="initEclipse" error "Unknown option ${1}"	
			return 9;;
		esac
		shift
	done

	if ${reset}; then
		unset ECLIPSE
		unset ECLIPSE_WORKSPACES_DIR
	fi

	if [[ -z "${ECLIPSE}" ]]; then 
		if [[ -e "${CommonProfile_services_eclipse_bin}" ]]; then
			export ECLIPSE="${CommonProfile_services_eclipse_bin}"
			${verbose} && log -name="initEclipse" info "Set ECLIPSE=${ECLIPSE}"
		elif [[ -d "/Applications/eclipse" ]]; then
			export ECLIPSE="$(ls -d "/Applications/eclipse/"* | tail -1)/Eclipse.app/Contents/MacOS/eclipse"
			${verbose} && log -name="initEclipse" info "Set ECLIPSE=${ECLIPSE}"
		fi
	fi
		
	if [[ -z "${ECLIPSE_WORKSPACES_DIR}" ]]; then
		export ECLIPSE_WORKSPACES_DIR="${HOME}/etc/workspaces"
		${verbose} && log -name="initEclipse" info "Set ECLIPSE_WORKSPACES_DIR=${ECLIPSE_WORKSPACES_DIR}"
	fi
}

function runEclipse() {
    declare workspace="${ECLIPSE_WORKSPACE:-${HOME}/workspaces/eclipse}"
    declare pid
	declare doOpen=true
	declare doPrint=false
	declare verbose=false

    while [[ ! -z ${1} ]]; do
		case "${1}" in
		-workspace=*)	workspace=$(fullyQualifiedPath "${1#*=}");;
		-id=*) 			workspace="${ECLIPSE_WORKSPACES_DIR}/${1#*=}" ;;
		-o)				doOpen=true;;
		-p)				doOpen=false; doPrint=true;;
		-v)				verbose=true;;
		*)      		log error "Unsupported option ${1}"; return 5;;
		esac

		shift 1
	done

	if [[ -z "${workspace}" ]]; then
		log error "runEclipse: Must specify workspace using -workspace or ECLIPSE_WORKSPACE"
		return 8
	elif [[ ! -d "${workspace}" ]]; then
		log error "runEclipse: Workspace does not exist: ${workspace}"
		return 8
	fi

	${doPrint} && log info "${ECLIPSE} -data \"${workspace}\" -showlocation ${args[@]}"

	if ${doOpen}; then
		declare pid
		if pid=$(getPid ""${ECLIPSE##*/}" -data ${workspace}"); then
			log error "Eclipse ${workspace} already running: ${pid}"
			return 1
		fi

		${verbose} && log info "${ECLIPSE} -data \"${workspace}\" -showlocation ${args[@]}"
		${doOpen} && ( nohup ${ECLIPSE} -data "${workspace}" -showlocation "${args[@]}" & ) >& /dev/null
	fi
}

function setupEclipse() {
	declare templateName='eclipse'
	declare targetName
	declare name
	declare templatePath
	declare workspace
	declare doOpen=true
	declare doPrint=false
	declare force=false
	declare verbose=false
	declare IFS=$'\n'

	if [[ "${ECLIPSE}" = *'/devstudio.app/'* ]]; then 
		templateName='devstudio'
	elif [[ "${ECLIPSE}" = *'/codereadystudio.app/'* ]]; then
		templateName='devstudio'
	fi

	while [[ ! -z "${1}"  ]]; do
		case "${1}" in
		-id=*)			targetName="${1#*=}";;
		-name=*)		targetName="${1#*=}";;
		-workspace=*)	workspace=$(fullyQualifiedPath "${1#*=}") ;;
		-template=*)	templateName="${1#*=}" ;;
		-eclipse)		templateName='eclipse' ;;
		-devstudo) 		templateName='devstudio' ;;
		-o)				doOpen=true;;
		-n|-p)			doOpen=false; doPrint=true;;
		-f)				force=true;;
		-v)				verbose=true;;
		*)  log error "Unsupported option ${1}"
			return 9;;
		esac

		shift 1
	done

	# If 
	if [[ -z "${targetName}" ]] && ! [[ -z "${ECLIPSE_WORKSPACE}" ]] && ! [[ -z "${ECLIPSE_WORKSPACES_DIR}" ]] ; then
		if [[ "${ECLIPSE_WORKSPACE}" = "${ECLIPSE_WORKSPACES_DIR}/"* ]]; then
			targetName="${ECLIPSE_WORKSPACE##*/}"
		fi
	fi
	
	[[ -z "${targetName}" ]] &&  targetName='workspace'
	[[ -z "${workspace}" ]] && workspace="${ECLIPSE_WORKSPACES_DIR}/${targetName}"


	if [[ -z "${templatePath}" ]]; then
		if ! templatePath=$(resolvePath "${CP_TEMPLATE_PATH}" "${templateName}"); then
			log "Cannot locate ${templateName} template"
			return 1
		fi
	fi

	${doPrint} && log info "${workspace}: ${templatePath}"

	if [[ -d "${workspace}" ]]; then
		if ${force}; then
			log info "Workspace ${workspace} exists"
		else
			log error "Workspace ${workspace} already exists"
			return 1
		fi
	else
		log info "Creating workspace directory ${workspace}"
		${doOpen} && mkdir -p "${workspace}/.metadata/.plugins/org.eclipse.core.runtime/.settings/"
	fi

	if [[ -d "${templatePath}/settings" ]]; then
		${verbose} && log info "Populating with settings from ${templatePath}"
		${doOpen} && cp -r "${templatePath}/settings"/* "${workspace}/.metadata/.plugins/org.eclipse.core.runtime/.settings/"
	fi

	if [[ -d "${templatePath}/setup" ]]; then
		for template in $(find "${templatePath}/setup" -type f); do
			${verbose} && log info "Setting ${template}"
			${doOpen} && "${template}" "-DECLIPSE_WORKSPACE=${workspace}"
		done
	fi

	if [[ -d "${templatePath}/launchers" ]]; then
		for template in $(find "${templatePath}/launchers" -type f); do
			${verbose} && log info "Launcher: ${template}"
			${doOpen} && "${template}" "-DECLIPSE_WORKSPACE=${workspace}"
		done
	fi
}
