#! /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# jsonlib.sh
####################################################################################################################################
#
# JSON functions
#
####################################################################################################################################

CP_IMPORTS+="json;"

function fromJsonString() {
	declare value="${1}"
	[[ "${value}" =~ \"(.*)\" ]] && value="${BASH_REMATCH[1]}"
	value="${value//\\r/}"
	value="${value//\\n/$'\n'}"
	value="${value//\\t/	}"
	echo -n "${value}"
}

#
# Merges all lines, escapes all quotes + newlines
#
function toJsonString() {
	if [[ -z "${1}" ]]; then
		echo -n "null"
	elif [[ "${1}" =~ ^[0-9]*$ ]]; then
		echo -n "${1}"
	elif [[ "${1}" = 'true' || "${value}"  = 'false' || "${value}" = '{' ]]; then
		echo -n "${1}"
	else
		declare value="${1}"
		value="${value//\"/\\\"}"
		value="${value//$'\n'/\\n}"
		value="${value//$'\t'/\\t}"
		value="${value///\\n}"
		echo -n "\"${value}\""
	fi
}

function getJsonValue() {
	declare info=true
	declare verbose=false
	declare mode='complex'
	declare showHelp=false

	while [[ "${1}" = -?* ]]; do
		case "${1}" in
		-h) showHelp=true;;
		-s)	info=false;;
		-n)	mode='scalar';;
		-v)	verbose=true;;
		-l) mode='keys';;
		-e) mode='entries';;
		*)	log -name="getJsonValue" error "Unknown option ${1}"
			return 2;;
		esac
		shift
	done

	
	declare json="${1}"
	declare key="${2}"
	declare value
	[[ "${json}" = '-' ]] && json="$(cat)"

	if ${showHelp}; then
		echo "getJsonValue"
		printKeyValueLine '-h'  'Display help'
		printKeyValueLine '-s'	'Silent operation'
		printKeyValueLine '-v'	'Verbose'
		printKeyValueLine '-n'	'Display scalar values only'
		printKeyValueLine '-l'	'List all keys'
		printKeyValueLine '-e'	'List all key/value pairs'

		if [[ -z "${1}" ]]; then
			return 0
		fi
	fi

	${verbose} && log info "Read ${key} from ${json}"

	case "${mode}" in
	complex)
		if ! value=$(echo "${json}" | jq "${key}" 2> /dev/null); then
			${info} && log -name="getJsonValue" info "Error reading ${key}"
			return 1
		fi
		if [[ "${value}" = 'null' ]]; then
			return 1
		fi

		[[ "${value}" != *$'\n'* ]] && [[ "${value}" =~ ^\"(.*)\"$ ]] && value="${BASH_REMATCH[1]}"
		;;
	scalar)
		if ! value=$(echo "${json}" | jq -re "${key} | select(type|.==\"string\"or.==\"boolean\"or.==\"number\")" 2> /dev/null); then
			${info} && log -name="getJsonValue" info "Cannot find value for key ${key}"
			return 1
		fi
		;;
	entries)
		if [[ -t 1 ]]; then
			echo "${json}" | jq -r "paths(scalars) as \$p | [ 
					( [ \"${TC_FG_GREEN}\" + \$p[] | tostring ] | join(\".\") + \"${TC_END}\") , 
					(  \"${TC_FG_WHITE}\" + (getpath(\$p) | tostring) + \"${TC_END}\") 
				] 
				| join(\": \")"
		else
			echo "${json}" | jq -r "paths(scalars) as \$p | [ 
					( [ \$p[] | tostring ] | join(\".\") ) , 
					(  (getpath(\$p) | tostring) ) 
				] 
				| join(\": \")"
		fi
		return 0
		;;
	keys)
		echo "${json}" | jq -r 'paths(scalars) | join(".")'
		return 0
		;;
	esac

	value="${value//\\r/}"
	value="${value//\\n/$'\n'}"
	value="${value//\\t/	}"
    echo -n "${value}"
}

function appendJson() {
	declare json="${1}"
	declare key="${2}"
	declare value="${3}"

	if [[ "${json: -1}" != '{' ]]; then
		json+=","
	fi

	json+="\"${key}\": $(toJsonString "${value}")"
	echo "${json}"
}
