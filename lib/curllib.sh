#! /bin/bash
# vim:ts=4:sw=4:
####################################################################################################################################
# curllib.sh
####################################################################################################################################
#
# Functions for working with CURL
#
# Sample usage:
# if ! httpResponse=$(runCurl --data-raw "${payload}" -H 'Content-Type: text/xml; charset = utf-8' -H 'SOAPAction: "#GetBindings"' -X POST "${defaultServiceUrl}"); then
#    log error "Fetch of ${defaultServiceUrl} failed"
#    httpHeader=( "$(getHttpHeader "${httpResponse}")" )
#    logHttpHeader "${httpHeader[@]}"
#    exit 1
# fi
# httpBody="$(getHttpBody "${httpResponse}")"
#
# echo "${httpBody}"  | xmllint --format - | highlight --out-format=ansi --syntax=xml
#
#
####################################################################################################################################

CP_IMPORTS+="curl;"

function urlEncode() {
	groovy -e 'println URLEncoder.encode(args[0],java.nio.charset.StandardCharsets.UTF_8.name())' "${1}"
}

function getHttpBody() {
	declare IFS=$'\n'
	declare results="${1}"
	declare moreHeader=true
	declare -i rc=1
	
	while ${moreHeader} && [[ "$(echo "${results}" 2> /dev/null | head -1)" =~ HTTP/[0-9.]+[[:space:]]([0-9]+)[[:space:]](.*) ]]; do
		declare -i httpReturnCode="${BASH_REMATCH[1]}"
		declare httpMessage="${BASH_REMATCH[2]}"

		if [[ ${httpReturnCode} -eq 100 ]]; then
			moreHeader=true
			results=$(echo "${results}" 2> /dev/null | sed -e '1,/^$/d')
		else
			rc=0
			moreHeader=false	
		fi
	done

	echo "${results}" 2> /dev/null | sed -e '1,/^$/d'
	return ${rc}
}

function getHttpHeader() {
	declare IFS=$'\n'
	declare results="${1}"
	declare lastHeader
	declare moreHeader=true
	declare -i rc=1

	while ${moreHeader} && [[ "$(echo "${results}" 2> /dev/null | head -1)" =~ HTTP/[0-9.]+[[:space:]]([0-9]+)[[:space:]](.*) ]]; do
		declare -i httpReturnCode="${BASH_REMATCH[1]}"
		declare httpMessage="${BASH_REMATCH[2]}"

		if [[ ${httpReturnCode} -eq 100 ]]; then
			moreHeader=true
			results=$(echo "${results}" | sed -e '1,/^$/d')
		else
			rc=0
			moreHeader=false
		fi
	done

	echo "${results}" 2> /dev/null | sed -e '/^$/q'
	return ${rc}
}

function printHttpHeader() {
	declare IFS=$'\n'
	declare payload="${1}"
	declare -a results=()

	declare header
	for header in ${payload}; do
		declare key="${header%:*}"
		declare value="${header#*:}"
		printKeyValueLine "${key}:" "$(trim "${value}")"
	done
}

function logHttpHeader() {
	declare IFS=$'\n'
	declare payload="${1}"
	declare -a results=()

	declare header
	for header in ${payload}; do
		log -name=curl info  " ... ${header}"
	done
}

function logHttpCookies() {
	declare IFS=$'\n'
	declare payload="${1}"
	declare -a results=()

	declare header
	for header in ${payload}; do
		log -name=curl info  " ... ${header}"
	done
}

function cpcurl() {
	declare IFS=$'\n'
	declare results

	if ! results=$(curl -s -D - "${@}"); then
		return 1
	fi

	# Strip CR's
	results="$(echo "${results}" | tr -d '\r')"

	# Create CP_HTTP_RC and CP_HTTP_MSG pseudo entries
	if [[ "$(echo "${results}" | head -1)" =~ HTTP/[0-9.]+[[:space:]]([0-9]+)[[:space:]](.*) ]]; then
		declare -i httpReturnCode="${BASH_REMATCH[1]}"
		declare httpMessage="${BASH_REMATCH[2]}"

		echo "CP_HTTP_RC:${httpReturnCode}"
		echo "CP_HTTP_MSG:${httpMessage}"
	fi

	echo "${results}"
	return 0
}

function oldRunCurn() {
	declare -a curlOptions=()
	declare debug=false
	declare debugOpt
	declare url=''

	while [[ ! -z "${1}" ]]; do
		case "${1}" in
		-d)	debug=true; debugOpt='-d';;
		*) 	if [[ ! -z "${2}" ]]; then
				curlOptions+=( "${1}" )
			else
				url="${1}"
			fi
		esac
		shift
	done

	declare results header
	if results=$(curl -c "${cookieJar}" -L -D - -s "${curlOptions[@]}" "${url}" | sed -e '//s///g'); then
		if header=$(getHttpHeader "${results}") && [[ "$(echo "${header}" 2> /dev/null | head -1)" =~ HTTP/[0-9.]+[[:space:]]([0-9]+)[[:space:]](.*) ]]; then
			declare -i httpReturnCode="${BASH_REMATCH[1]}"
			declare httpMessage="${BASH_REMATCH[2]}"

			if [[ ${httpReturnCode} -ge 200 && ${httpReturnCode} -le 299 ]]; then
				if ${debug}; then
					log -name=curl info "SUCCESS: ${url}"
					logHttpHeader "${header}"
				fi
				echo "${results}"
				return 0
			else
				if ${debug}; then
					log -name=curl error "curl failed with \"${httpMessage}\" (${httpReturnCode})  while requesting ${url}"
					logHttpHeader "${header}"
				fi
				echo "${results}"
				return 1
			fi
		else
			if ${debug}; then
				log info "Cannot parse HTTP return code from \"$(echo "${results}" | head -1)\"  while requesting ${url}"
			fi
			return 2
		fi
	else
		declare rc=$?
		log -name=curl error "curl failed with ${rc} while requesting ${url}"
		echo "${header}"
		echo "${results}"
		return ${rc}
	fi
}

