#! /bin/bash 
####################################################################################################################################
# backup library
####################################################################################################################################

CP_IMPORTS+="backup;"

#
# Copy all files from the source directory to the destination directory
#
function backupViaCopy() {
	declare IFS=$'\n'
	declare -a opts=( '-a' )
	declare -a exclude=( '--exclude' '.DS_Store' )
	declare verbose=false
	declare silent=false
	declare dryRun=false
	declare mkIfNeeded=false
	declare -a srcDirs=()

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-exclude=*|-e=*)	exclude+=( '--exclude' "${1#*=}" ) ;;
		-include=*|-i=*)	exclude+=( '--include' "${1#*=}" ) ;;
		-d)					opts+=( "--delete" );;
		-p) 				mkIfNeeded=true;;
		-v) 				verbose=true;;
		-s) 				silent=true;;
		-n)					dryRun=true;;
		*)
			log -name="backup" error "Unknown option ${1}"
			return 1;
		esac

		shift
	done

	if ${verbose}; then
		opts+=( '-v' )
	elif ${silent}; then
		opts+=( '-q' )
	fi
	

	declare destDir="${1}"; shift
	declare -a srcDirs=( "${@}" )

	for srcDir in ${srcDirs[@]}; do
		${verbose} && log info "Source dir: ${srcDir}"
		if [[ ! -e "${srcDir}" ]]; then
			log -name="backup" error "Source ${srcDir} does not exist"
			return 1
		fi
	done

	if [[ ! -d "${destDir}" ]]; then
		${verbose} && log info "Dest dir: ${destDir}"
		if ${mkIfNeeded}; then
			if ! mkdir -p "${destDir}"; then
				log -name="backup" error "Unable to make destination dir: ${destDir}"
				return 1
			fi
		fi
	fi

	if [[ ! -d "${destDir}" ]]; then
		log -name="backup" error "Destination directory ${destDir} does not exist"
		return 1
	fi

	${verbose} && log -name=backup info "rsync ${opts[@]} ${exclude[@]} ${srcDirs[@]} ${destDir}"
	! ${dryRun} && rsync ${opts[@]} ${exclude[@]} ${srcDirs[@]} "${destDir}"

	return 0
}

function backupViaTar() {
	declare IFS=$'\n'
	declare -a opts=( -c -j )
	declare -a exclude=( '--exclude' '.DS_Store' )
	declare verbose=false
	declare silent=false
	declare dryRun=false
	declare mkIfNeeded=false
	declare srcDir

	while [[ "${1}" = -* ]]; do
		case "${1}" in
		-exclude=*|-e=*)
			exclude+=( '--exclude' "${1#*=}" )
			;;
		-include=*|-i=*)
			exclude+=( '--include' "${1#*=}" )
			;;
		-C=*)
			srcDir="${1#*=}"
			opts+=( '-C' "${srcDir}" )
			;;
		-show)
			printName=true;;
		-p)
			mkIfNeeded=true;;
		-v)
			verbose=true;;
		-s)
			silent=true;;
		-n)
			dryRun=true;;
		*)
			log -name="backup" error "Unknown option ${1}"
			return 1;
		esac

		shift
	done

	if ${verbose}; then
		opts+=( -v )
	fi
	

	declare destDir="${1}"; shift
	declare keyName="${1}"; shift
	declare srcFiles="${@}"

	for srcFile in ${srcFiles[@]}; do
		if [[ ! -e "${srcDir:+${srcDir}/}${srcFile}" ]]; then
			log -name="backup" error "Source ${srcFile} does not exist"
			return 1
		fi
	done

	if [[ ! -d "${destDir}" ]]; then
		if ${mkIfNeeded}; then
			if ! mkdir -p "${destDir}"; then
				log -name="backup" error "Unable to make destination dir: ${destDir}"
				return 1
			fi
		fi
	fi

	if [[ ! -d "${destDir}" ]]; then
		log -name="backup" error "Destination directory ${destDir} does not exist"
		return 1
	fi

	declare destFile="${destDir}/${keyName}-$(date +"%Y-%m-%d-%H:%M:%S").tar.bz2"

	! ${silent} && echo "${destFile}"
	${verbose} && log -name=backup info "tar ${opts[@]} ${exclude[@]} -cjf \"${destFile}\" ${srcFiles[@]}"
	! ${dryRun} && tar "${opts[@]}" "${exclude[@]}" -f "${destFile}" ${srcFiles[@]}

	return 0
}
