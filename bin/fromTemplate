#! /bin/bash
####################################################################################################################################
# fromTemplate
####################################################################################################################################
# Copies a template to a file
####################################################################################################################################

[[ ";${CP_IMPORTS};" != *';common;' ]] && source "${CP_BASE:=${HOME}/Profile}/Common/lib/commonlib.sh"

declare verbose=false
declare templateFile
declare resultFile
declare backupOldFile=true
declare setAttribute=false
declare doMkdir=false
declare -a variables=()

while [[ ! -z "${1}" ]]; do
	case "${1}" in
	-x)	setAttribute=true;;
	-p)	doMkdir=true;;
	-v)	verbose=true;;
	-f)	backupOldFile=false;;
	-D*)
		declare var="${1#*-D}"
		declare key="${var%%=*}"
		declare value="${var#*=}"
		variables+=( "${key}:${value}" )
		eval "${key}=\"${value}\""
		;;
	*) 	[[ -z "${templateFile}" ]] && templateFile="${1}" || resultFile="${1}";;
	esac
	shift 1
done

if [[ ! -r "${templateFile}" ]]; then
	log error "Cannot locate template: ${templateFile}"
	exit 1
fi

${verbose} && log info "Reading ${templateFile}"

declare header=$(sed -E -e '/^$/,$ d' -e '/^#!/d' -e '/^##/d' -e 's/^# *//' -e 's/:[[:space:]]*/:/' "${templateFile}" )
declare template=$(sed '1,/^$/d' "${templateFile}")


declare IFS=$'\n'
declare entry key value
for entry in ${header}; do
	key=$(trim "${entry%%:*}")
	value=$(trim "${entry#*:}" )
	value=$(eval "echo \"${value}\"")

	case "${key}" in
		Option.mkdir)		doMkdir=true;;
		Option.verbose)		verbose=true;;
		Option.backup)		backupOldFile=true;;
		Option.nobackup)	backupOldFile=false;;
		Option.xattr)		setAttribute=true;;
		Option.*)			
			log error "Unknown option ${key}"
			exit 1
			;;
	esac

	if [[ ! -z "${key}" ]] && ! getMapValue "${key}" "${variables[@]}" &> /dev/null; then
		${verbose} && log info "Registering ${key}=${value}"
		variables+=( "${key}:${value}" )
		[[ "${key}" != *'.'* ]] && eval "${key}=\"${value}\""

		if [[ -z "${resultFile}" && "${key}" = 'Template.destination' ]]; then
			resultFile="${value}"
		fi
	fi
done

variables+=( "Template.generation.source:$(fullyQualifiedPath "${templateFile}")" )
variables+=( "Template.generation.destination:${resultFile}" )
variables+=( "Template.generation.timestamp:$(date)" )
variables+=( "Template.generation.date:$(date +"%Y-%m-%d")" )
variables+=( "Template.generation.time:$(date +"%H:%M:%S")" )
variables+=( "Template.generation.by:$(whoami)" )

if ${verbose}; then
	for var in ${variables[@]}; do
		log info "Variable: ${var}"
	done
fi

if [[ "${resultFile:=-}" = '-' ]]; then
	${verbose} && log info "Writing to stdout"
	varSub "${template}" "${variables[@]}"
else
	if ${doMkdir}; then
		declare dir=$(dirname "${resultFile}")
		if [[ ! -d "${dir}" ]]; then
			${verbose} && log info "mkdir ${dir}"
			mkdir -p "${dir}"
		fi
	fi
	if [[ -r "${resultFile}" ]] && ${backupOldFile}; then
		declare backupDir=$(dirname $(fullyQualifiedPath "${resultFile}"))
		declare backupName=$(basename "${resultFile%.*}")
		declare backupFile="${backupDir}/${backupName}-$(date +"%Y%m%d%H%M%S").bak"
		if ! cp "${resultFile}" "${backupFile}"; then
			log info "Backup of ${resultFile} to ${backupFile} failed"
			exit 2
		fi
	fi
	${verbose} && log info "Writing to ${resultFile}"
	varSub "${template}" "${variables[@]}" > "${resultFile}"

	if [[ "${resultFile}" != '-' ]]; then
		declare templateMode=''
		if templateMode="$(getMapValue 'Template.chmod' "${variables[@]}")"; then
			log debug "Change mode to ${templateMode}"
			chmod "${templateMode}" "${resultFile}"
		fi

		if ${setAttribute}; then
			xattr -w 'Template.source' "$(fullyQualifiedPath "${templateFile}")" "${resultFile}"
		fi
	fi

fi
