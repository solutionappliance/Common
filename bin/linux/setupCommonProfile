#! /bin/bash
# vim:ts=4:sw=4:ai:
####################################################################################################################################
# setupCommonProfile
####################################################################################################################################
#
# Installs the necessary software to setup a Linux machine.
#
####################################################################################################################################

# Install needed tools
if command -v apt &> /dev/null; then
	for tool in curl vim highlight unzip git jq; do
		if ! command -v ${tool} &> /dev/null; then
			echo " ... install ${tool}"
			sudo apt -q install -y ${tool}
		else
			echo ".... ${tool} installed"
		fi
	done
elif command -v  apk &> /dev/null; then
	for tool in curl vim highlight unzip git jq; do
		if ! command -v ${tool} &> /dev/null; then
			echo " ... install ${tool}"
			sudo apk add ${tool}
		else
			echo ".... ${tool} installed"
		fi
	done
fi



# Common-profile
if [[ ";${CP_IMPORTS};" != *";common;"* ]]; then
	[[ -z "${CP_BASE}" ]] && export CP_BASE="${HOME}/Profile"

	if [[ ! -d "${CP_BASE}/Common" ]]; then
		if ! git clone https://gitlab.com/solutionappliance/Common.git "${CP_BASE}/Common"; then
			echo "Cannot clone Common Profile!"
			exit 1
		fi
	fi

	if [[ ! -d "${CP_BASE}/Common" ]]; then
		echo "Cannot find ${CP_BASE}/Common"
		exit 1
	fi

	echo " ... initCommonProfile"
	source "${CP_BASE}/Common/lib/profilelib.sh" || exit 1
	initCommonProfile -default=';cp=true;' -src
fi

# SUDOers
log info "Checking if need to add $(whoami) to /etc/sudoers"
if ! sudo grep -E "^$(whoami)" /etc/sudoers &> /dev/null; then
	log info "Need to add $(whoami) to /etc/sudoers"
sudo sed -i '' -e "/^%admin/a\\
$(whoami)       ALL = (ALL) NOPASSWD: ALL" /etc/sudoers
else
	log info "$(whoami) already in /etc/sudoers"
fi


# .cpprofile?
declare cpprofileTemplate="$(resolvePath "${CP_TEMPLATE_PATH}" cpprofile)"
if [[ ! -f "${HOME}/.cpprofile" ]]; then
	log info "Creating ~/.cpprofile from ${cpprofileTemplate}"
	"${cpprofileTemplate}"
else
	log info "cpprofile already exists (template=${cpprofileTemplate})"
fi

#  .bashrc
declare bashrcTemplate="$(resolvePath "${CP_TEMPLATE_PATH}"  bashrc)"
if [[ ! -f "${HOME}/.bashrc" ]] || ! grep initCommonProfile "${HOME}/.bashrc" &> /dev/null; then
	log info "Updating ~/.bashrc"
	"${bashrcTemplate}"
else
	log info "bashrc exists and is good"
fi


# .bash_profile
declare bashProfileTemplate="$(resolvePath "${CP_TEMPLATE_PATH}"  bash_profile)"
if [[ ! -f "${HOME}/.bash_profile" ]] || ! grep .bashrc "${HOME}/.bash_profile" &> /dev/null; then
	log info "Updating ~/.bash_profile"
	"${bashProfileTemplate}"
else
	log info "bash_profile exists and is good"
fi

