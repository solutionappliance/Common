# Solution Appliance Common Profile

## Mac Setup Instructions
Clone the Common files into your ~/Profile directory

```
mkdir ~/Profile
cd ~/Profile
git clone git@gitlab.com:solutionappliance/Common.git Common
```

Run the setup script

```
setupCommonProfile
```

Open a new terminal window
